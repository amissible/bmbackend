<p>Hi ${name},</p>
<p>There is an update regarding your consignment.</p>
<table style="border-collapse: collapse; width: 100%;" border="1">
<tbody>
<tr>
<td style="width: 30%;"><strong>${empName} :&nbsp;</strong></td>
<td style="width: 70%;">&nbsp;</td>
</tr>
<tr>
<td style="width: 30%;">&nbsp;</td>
<td style="width: 70%; background-color: #e8f0fe; padding: 10px;">${update}</td>
</tr>
</tbody>
</table>