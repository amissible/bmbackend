<p>Hi ${name},</p>
<p>There is a refund on your Consignment.</p>
<table style="border-collapse: collapse; width: 100%; height: 51px;" border="1">
<tbody>
<tr style="height: 17px;">
<td style="width: 30%; height: 17px;"><strong>Consignment #</strong></td>
<td style="width: 70%; height: 17px;">${consignmentId}</td>
</tr>
<tr style="height: 17px;">
<td style="width: 30%; height: 17px;"><strong>Refund Amount</strong></td>
<td style="width: 70%; height: 17px;">${refundAmount}</td>
</tr>
<tr style="height: 17px;">
<td style="width: 30%; height: 17px;"><strong>Refund Description</strong></td>
<td style="width: 70%; height: 17px;">${refundDesc}</td>
</tr>
</tbody>
</table>