<p>Hi {{name}},</p>
<p>You have made a successful payment. You can download the receipt from Payment section. Have a nice day.</p>
<p><strong>Transaction ID</strong> : {{txnId}}</p>
<p><strong>Transaction Amount</strong> : {{amt}}</p>
<p><strong>Date and Time</strong> : {{timeStamp}}</p>