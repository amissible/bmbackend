package com.amissible.movingsolutions.util;

import com.amissible.movingsolutions.exception.AppException;
import net.fortuna.ical4j.model.*;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.parameter.*;
import net.fortuna.ical4j.model.property.*;
import net.fortuna.ical4j.util.UidGenerator;
import org.apache.commons.codec.binary.Base32;
import org.springframework.http.HttpStatus;

import java.io.*;
import java.net.URI;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Utility {

    public static String generateNumericOTP(Integer length){
        String OTP = "";
        for(int i=1;i<=length;i++)
            OTP = OTP + (int)(Math.random()*10);
        return OTP;
    }

    public static String generateAlphaNumericOTP(Integer length){
        String OTP = "";
        for(int i=1;i<=length;i++){
            if(i%2 == 0)
                OTP = OTP + (char)(65+Math.random()*26);
            else
                OTP = OTP + (int)(Math.random()*10);
        }
        return OTP;
    }

    public static String generateRandomCode() {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[20];
        random.nextBytes(bytes);
        Base32 base32 = new Base32();
        return base32.encodeToString(bytes);
    }

    public static String getDayOfWeek(Integer i){
        switch(i){
            case Calendar.MONDAY: return "MONDAY";
            case Calendar.TUESDAY: return "TUESDAY";
            case Calendar.WEDNESDAY: return "WEDNESDAY";
            case Calendar.THURSDAY: return "THURSDAY";
            case Calendar.FRIDAY: return "FRIDAY";
            case Calendar.SATURDAY: return "SATURDAY";
            case Calendar.SUNDAY: return "SUNDAY";
        }
        return "";
    }

    public static void generateICS(Long appointmentId,String eventName,String description,String location,String hostName,String hostEmail, String guestName,String guestEmail,String timeZone,String date,String startTime,String endTime,String path){
        try {
            Integer year = Integer.parseInt(date.substring(0,4));
            Integer month = Integer.parseInt(date.substring(5,7));
            Integer day = Integer.parseInt(date.substring(8,10));
            Integer startHour = Integer.parseInt(startTime.substring(0,2));
            Integer startMin = Integer.parseInt(startTime.substring(3,5));
            Integer endHour = Integer.parseInt(endTime.substring(0,2));
            Integer endMin = Integer.parseInt(endTime.substring(3,5));

            TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
            TimeZone timezone = registry.getTimeZone(timeZone);
            VTimeZone tz = timezone.getVTimeZone();

            Calendar startDate = new GregorianCalendar();
            startDate.setTimeZone(timezone);
            startDate.set(java.util.Calendar.MONTH, month-1);
            startDate.set(java.util.Calendar.DAY_OF_MONTH, day);
            startDate.set(java.util.Calendar.YEAR, year);
            startDate.set(java.util.Calendar.HOUR_OF_DAY, startHour);
            startDate.set(java.util.Calendar.MINUTE, startMin);
            startDate.set(java.util.Calendar.SECOND, 0);

            Calendar endDate = new GregorianCalendar();
            endDate.setTimeZone(timezone);
            endDate.set(java.util.Calendar.MONTH, month-1);
            endDate.set(java.util.Calendar.DAY_OF_MONTH, day);
            endDate.set(java.util.Calendar.YEAR, year);
            endDate.set(java.util.Calendar.HOUR_OF_DAY, endHour);
            endDate.set(java.util.Calendar.MINUTE, endMin);
            endDate.set(java.util.Calendar.SECOND, 0);

            DateTime start = new DateTime(startDate.getTime());
            DateTime end = new DateTime(endDate.getTime());
            VEvent meeting = new VEvent(start, end, eventName);
            meeting.getProperties().add(tz.getTimeZoneId());

            UidGenerator ug = new UidGenerator("APPOINTMENT-"+appointmentId);
            Uid uid = ug.generateUid();
            meeting.getProperties().add(uid);

            Organizer  organizer = new Organizer(URI.create("mailto:"+hostEmail));
            organizer.getParameters().add(new Cn(hostName));
            meeting.getProperties().add(organizer);

            Attendee attendee = new Attendee(URI.create("mailto:"+guestEmail));
            attendee.getParameters().add(Role.REQ_PARTICIPANT);
            attendee.getParameters().add(new Cn(guestName));
            attendee.getParameters().add(PartStat.NEEDS_ACTION);
            attendee.getParameters().add(Rsvp.TRUE);
            meeting.getProperties().add(attendee);

            meeting.getProperties().add(new Description(description));
            meeting.getProperties().add(new Location(location));
            meeting.getProperties().add(Status.VEVENT_CONFIRMED);
            meeting.getProperties().add(Transp.OPAQUE);

            net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
            icsCalendar.getProperties().add(new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
            icsCalendar.getProperties().add(CalScale.GREGORIAN);
            icsCalendar.getComponents().add(meeting);
            BufferedWriter writer = new BufferedWriter(new FileWriter(new File(path)));
            writer.write(icsCalendar.toString());
            writer.close();
        }
        catch (Exception e){
            throw new AppException("ERROR_GENERATING_ICS", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}