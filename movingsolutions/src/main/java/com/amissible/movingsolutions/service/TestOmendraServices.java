package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.config.email.Publisher;
import com.amissible.movingsolutions.config.jwt.JWTFilter;
import com.amissible.movingsolutions.config.jwt.TokenProvider;
import com.amissible.movingsolutions.dao.*;
import com.amissible.movingsolutions.exception.AppException;
import com.amissible.movingsolutions.model.*;
import com.amissible.movingsolutions.payload.Questionary;
import io.jsonwebtoken.Claims;
import io.swagger.models.auth.In;
import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TestOmendraServices {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ConsignmentDao consignmentDao;

    @Autowired
    private QuestionDao questionDao;

    @Autowired
    private QuestionnaireDao questionnaireDao;

    @Autowired
    private PaymentDao paymentDao;

    @Autowired
    private InvoiceDao invoiceDao;

    @Autowired
    private InvoiceRecordDao invoiceRecordDao;

    @Autowired
    private ContractDao contractDao;

    @Autowired
    private ComplaintDao complaintDao;

    @Autowired
    private FeedbackDao feedbackDao;

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private EmployeeTaskDao employeeTaskDao;

    @Autowired
    private SettingDao settingDao;

    @Autowired
    private TaskDao taskDao;

    @Autowired
    TokenProvider tokenProvider;

    @Autowired
    JWTFilter jwtFilter;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    ReportService reportService;

    @Autowired
    PdfDownloadService pdfDownloadService;

    @Autowired
    Publisher publisher;

    private static final Logger logger = LoggerFactory.getLogger(TestOmendraServices.class);

    // BM-16
    public User getUserDetails(Long id){
        User user = userDao.getById(id);
        if(user == null)
            throw new AppException("User not found", HttpStatus.NOT_FOUND);
        user.setPassword("");
        return user;
    }
    public User getUserDetailsByEmail(String email){
        return userDao.getByEmail(email);
    }
    public void updateUserDetails(User user){
        if(user.getId() == null)
            throw new AppException("User ID cannot be empty",HttpStatus.BAD_REQUEST);
        User dbUser = userDao.getById(user.getId());
        if(dbUser == null)
            throw new AppException("User not found", HttpStatus.NOT_FOUND);
        if(user.getName() == null)
            throw new AppException("Name cannot be empty",HttpStatus.BAD_REQUEST);
        dbUser.setName(user.getName());
        if(user.getContact() == null)
            throw new AppException("Contact cannot be empty",HttpStatus.BAD_REQUEST);
        dbUser.setContact(user.getContact());
        System.out.println(user.getPassword().equals(dbUser.getPassword()));
        System.out.println(encoder.encode(user.getPassword()).equals(dbUser.getPassword()));
        if(!(user.getPassword().equals(dbUser.getPassword()) || encoder.encode(user.getPassword()).equals(dbUser.getPassword())))
            dbUser.setPassword(encoder.encode(user.getPassword()));
        if(user.getTimeZone() == null)
            throw new AppException("TimeZone cannot be empty",HttpStatus.BAD_REQUEST);
        dbUser.setTimeZone(user.getTimeZone());
        userDao.update(dbUser);
        logger.info("Details Updated for User "+dbUser.getId());
    }
    public void updateUserPassword(Long id,String oldPassword,String newPassword){
        User user = userDao.getById(id);
        if(user == null)
            throw new AppException("User not found", HttpStatus.NOT_FOUND);
        if(!user.getPassword().equals(encoder.encode(oldPassword)))
            throw new AppException("Old Password is incorrect",HttpStatus.BAD_REQUEST);
        if(newPassword == null || newPassword.length() == 0)
            throw new AppException("New Password cannot be empty",HttpStatus.NOT_ACCEPTABLE);
        user.setPassword(encoder.encode(newPassword));
        userDao.update(user);
        logger.info("Password changed for User "+id);
    }

    // BM-20
    public RedirectView makePayment(Long consignmentId,Float amount){
        settingDao.refresh();
        return new RedirectView(settingDao.get("PaymentPath")+"?alias="+settingDao.get("PaymentAlias")+"&amount="+amount+"&currency="+settingDao.get("Currency")+"&token="+consignmentId);
    }
    public RedirectView success(Long consignmentId,String state,Float txnAmount,String txnId){
        Consignment consignment = consignmentDao.getById(consignmentId);
        if(consignment == null)
            throw new AppException("Consignment Not Found",HttpStatus.NOT_FOUND);
        User user = userDao.getById(consignment.getUserId());
        if(user == null)
            throw new AppException("User Not Found",HttpStatus.NOT_FOUND);
        Payment payment = new Payment();
        payment.setUserId(consignment.getUserId());
        payment.setConsignmentId(consignmentId);
        payment.setTxnId(txnId);
        payment.setTxnAmount(txnAmount);
        if(state.equalsIgnoreCase("completed"))
            payment.setTxnStatus("SUCCESS");
        else
            payment.setTxnStatus("UNCLEAR");
        settingDao.refresh();
        payment.setTxnTimeStamp(new SimpleDateFormat(settingDao.get("DateTimeFormat")).format(new Date()));
        paymentDao.save(payment);
        // Trigger a mail to user
        Mail mail = new Mail();
        mail.setTo(user.getEmail());
        mail.setCc("amit.shukla@amissible.com");
        mail.setBcc("anwar.mehmood@amissible.com");
        mail.setSubject("Congratulations, you have made a Payment");
        mail.setBody("AdminPayment.ftl");
        Map<String,String> map = new HashMap<>();
        map.put("name", user.getName());
        map.put("txnId", payment.getTxnId());
        map.put("amt", settingDao.get("Currency") + " " + payment.getTxnAmount().toString());
        map.put("timeStamp", payment.getTxnTimeStamp());
        mail.setModel(map);
        try{
            publisher.setMailBlockingQueue(mail);
        }
        catch (Exception e){
            logger.error("FAILED TO SEND PAYMENT EMAIL : ERROR => "+e.getMessage());
        }
        return new RedirectView(settingDao.get("SuccessPayPath"));
    }
    public RedirectView cancel(Long consignmentId){
        return new RedirectView(settingDao.get("FailurePayPath"));
    }

    // BM-30
    public List<Payment> getPaymentData(){
        return paymentDao.getAll();
    }

    // BM-25
    public List<Complaint> getComplaintData(){
        return complaintDao.getAll();
    }
    public void updateComplaint(Complaint complaint){
        complaintDao.update(complaint);
        //Email
    }

    // BM-28
    public List<Feedback> getFeedbackData(){
        return feedbackDao.getAll();
    }

    //BM-33
    public User getUser(Long userId){
        return userDao.getById(userId);
    }
    public List<User> getUserData(){
        return userDao.getAll();
    }
    public void updateUser(User user){
        if(user.getId() == null)
            throw new AppException("User ID cannot be empty",HttpStatus.BAD_REQUEST);
        User dbUser = userDao.getById(user.getId());
        if(dbUser == null)
            throw new AppException("User not found", HttpStatus.NOT_FOUND);
        if(user.getName() == null)
            throw new AppException("Name cannot be empty",HttpStatus.BAD_REQUEST);
        dbUser.setName(user.getName());
        if(user.getContact() == null)
            throw new AppException("Contact cannot be empty",HttpStatus.BAD_REQUEST);
        dbUser.setContact(user.getContact());
        if(!user.getPassword().equals(dbUser.getPassword()))
            dbUser.setPassword(encoder.encode(user.getPassword()));
        if(user.getTimeZone() == null)
            throw new AppException("TimeZone cannot be empty",HttpStatus.BAD_REQUEST);
        dbUser.setTimeZone(user.getTimeZone());
        userDao.update(dbUser);
        logger.info("Details Updated for User "+dbUser.getId());
    }

    //BM-
    public List<Employee> getEmployeeData(){
        return employeeDao.getAll();
    }
    public void updateEmployee(Employee employee){
        employee.setPassword(encoder.encode(employee.getPassword()));
        employeeDao.update(employee);
    }
    public Employee saveEmployee(Employee employee){
        employee.setPassword(encoder.encode(employee.getPassword()));
        Employee emp = employeeDao.getByEmail(employee.getEmail());
        if(emp != null)
            throw new AppException("Email not available",HttpStatus.BAD_REQUEST);
        settingDao.refresh();
        employee.setTimeStamp(new SimpleDateFormat(settingDao.get("DateTimeFormat")).format(new Date()));
        employeeDao.save(employee);
        emp = employeeDao.getByEmail(employee.getEmail());
        if(emp == null)
            throw new AppException("Employee could not be saved",HttpStatus.INTERNAL_SERVER_ERROR);
        logger.info("NEW EMPLOYEE ADDED : "+emp.toString());
        return emp;
    }
    public List<Employee> getEmployeeByEmail(String email){
        System.out.println("getEmployeeByEmail");
        List<Employee> employees = new ArrayList<>();
        Employee emp = employeeDao.getByEmail(email);
        System.out.println("BB ");
        if(emp == null)
            throw new AppException("Employee could not be saved",HttpStatus.INTERNAL_SERVER_ERROR);
        employees.add(emp);
        System.out.println("B");
        return employees;
    }

    //BM-
    public List<Invoice> getInvoiceData(){
        return invoiceDao.getAll();
    }
    public void updateInvoice(Invoice invoice){
        if(invoice == null)
            throw new AppException("Empty Data Set cannot be updated",HttpStatus.BAD_REQUEST);
        invoice.calculateTotal();
        invoiceDao.update(invoice);
    }
    public void saveInvoice(Invoice invoice){
        if(invoice == null)
            throw new AppException("Empty Data Set cannot be saved",HttpStatus.BAD_REQUEST);
        invoice.calculateTotal();
        invoiceDao.save(invoice);
    }
    public List<InvoiceRecord> getInvoiceRecordData(Long invoiceId){
        return invoiceRecordDao.getAllByInvoiceId(invoiceId);
    }
    public void updateInvoiceRecord(InvoiceRecord invoiceRecord){
        if(invoiceRecord == null)
            throw new AppException("Empty Data Set cannot be updated",HttpStatus.BAD_REQUEST);
        invoiceRecord.calculateAmount();
        invoiceRecordDao.update(invoiceRecord);
    }
    public void deleteInvoiceRecord(InvoiceRecord invoiceRecord){
        if(invoiceRecord == null)
            throw new AppException("Empty Data Set cannot be deleted",HttpStatus.BAD_REQUEST);
        invoiceRecord.calculateAmount();
        invoiceRecordDao.delete(invoiceRecord.getId());
        Invoice invoice = invoiceDao.getById(invoiceRecord.getInvoiceId());
        invoice.setSubTotal(invoice.getSubTotal()-invoiceRecord.getAmount());
        invoice.calculateTotal();
        invoiceDao.update(invoice);
    }
    public InvoiceRecord saveInvoiceRecord(InvoiceRecord invoiceRecord){
        if(invoiceRecord == null)
            throw new AppException("Empty Data Set cannot be saved",HttpStatus.BAD_REQUEST);
        invoiceRecord.calculateAmount();
        invoiceRecordDao.save(invoiceRecord);
        List<InvoiceRecord> invoiceRecords = invoiceRecordDao.getAllByInvoiceId(invoiceRecord.getInvoiceId());
        Integer size = invoiceRecords.size();
        if(size == 0)
            throw new AppException("Invoice Item not found",HttpStatus.NOT_FOUND);
        Invoice invoice = invoiceDao.getById(invoiceRecord.getInvoiceId());
        invoice.setSubTotal(invoice.getSubTotal()+invoiceRecord.getAmount());
        invoice.calculateTotal();
        invoiceDao.update(invoice);
        //if(!invoiceRecords.get(size-1).equals(invoiceRecord))
        //    throw new AppException("Invoice Item Data Mismatch",HttpStatus.EXPECTATION_FAILED);
        return invoiceRecords.get(size-1);
    }
    public ResponseEntity generateInvoicePDF(Long invoiceId){
        settingDao.refresh();
        logger.info("Generating PDF for Invoice "+invoiceId);
        Invoice invoice = invoiceDao.getById(invoiceId);
        if(invoice == null)
            throw new AppException("Invoice does not exist",HttpStatus.NOT_FOUND);
        List<InvoiceRecord> invoiceRecords = invoiceRecordDao.getAllByInvoiceId(invoiceId);

        InvoicePayload payload = new InvoicePayload();
        payload.setId(invoice.getId());
        payload.setConsignmentId(invoice.getConsignmentId());
        payload.setBillAddress(invoice.getBillAddress());
        payload.setShipAddress(invoice.getShipAddress());
        payload.setDate(invoice.getDate());
        payload.setDueDate(invoice.getDueDate());
        payload.setStatus(invoice.getStatus());
        payload.setSubTotal(invoice.getSubTotal());
        payload.setDiscount(invoice.getDiscount());
        payload.setTax(invoice.getTax());
        payload.setTotal(invoice.getTotal());
        payload.setInvoiceRecordList(invoiceRecords);
        try {
            reportService.exportInvoice("pdf",payload,settingDao.get("ExportPath"));
            return pdfDownloadService.downloadPdf("/app/export/movsol/invoice/invoice_"+payload.getId()+".pdf");
        } catch (Exception e) {
            logger.error("INVOICE "+payload.getId()+" PDF ERROR :"+e.getMessage());
            throw new AppException("PDF Generation failed",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    public ResponseEntity generatePaymentPDF(Long paymentId){
        settingDao.refresh();
        Payment payment = paymentDao.getById(paymentId);
        try{
            reportService.exportPayment("pdf",payment,settingDao.get("ExportPath"));
            return pdfDownloadService.downloadPdf(settingDao.get("ExportPath")+"/payment/payment_"+paymentId+".pdf");
        }
        catch (Exception e){
            logger.error("PAYMENT "+paymentId+" PDF ERROR :"+e.getMessage());
            throw new AppException("PDF Generation failed",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    public ResponseEntity generateContractPDF(Long contractId){
        settingDao.refresh();
        Contract contract = contractDao.getById(contractId);
        try{
            reportService.exportContract("pdf",contract,settingDao.get("ExportPath"));
            contract.setUrl(settingDao.get("ExportPath")+"/contract/contract_"+contractId+".pdf");
            contractDao.update(contract);
            return pdfDownloadService.downloadPdf(contract.getUrl());
        }
        catch (Exception e){
            logger.error("CONTRACT "+contractId+" PDF ERROR :"+e.getMessage());
            throw new AppException("PDF Generation failed",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    public ResponseEntity getSignedContractPDF(Long contractId){
        settingDao.refresh();
        File file = new File(settingDao.get("ExportPath")+"/contract/contract_"+contractId+".pdf");
        if(!file.exists())
            throw new AppException("Contract not generated yet",HttpStatus.NOT_FOUND);
        file = new File(settingDao.get("ExportPath")+"/contract/signed_contract_"+contractId+".pdf");
        if(!file.exists())
            throw new AppException("Contract not signed yet",HttpStatus.NOT_FOUND);
        try{
            return pdfDownloadService.downloadPdf(settingDao.get("ExportPath")+"/contract/signed_contract_"+contractId+".pdf");
        }
        catch (Exception e){
            logger.error("SIGNED CONTRACT "+contractId+" PDF ERROR :"+e.getMessage());
            throw new AppException("Failed to obtain signed contract",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public Consignment getConsignment(Long consignmentId){
        return consignmentDao.getById(consignmentId);
    }
    public List<Consignment> getConsignmentData(){
        return consignmentDao.getAll();
    }
    public void updateConsignment(Consignment consignment){
        consignmentDao.update(consignment);
    }

    public Contract getContract(Long consignmentId){
        Contract contract = contractDao.getByConsignmentId(consignmentId);
        if(contract == null){
            Contract contract1 = new Contract();
            contract1.setConsignmentId(consignmentId);
            contractDao.save(contract1);
            contract = contractDao.getByConsignmentId(consignmentId);
        }
        return contract;
    }
    public void updateContract(Contract contract){contractDao.update(contract);}

    public List<Questionary> getQuestionary(Long consignmentId){
        /*if(consignmentId == 46)
        throw new AppException("Hello error",HttpStatus.NOT_FOUND);*/
        List<Questionary> questionaries = new ArrayList<>();
        Map<Long,String> questionMap = new HashMap<>();
        questionDao.getAll().forEach(question -> questionMap.put(question.getId(),question.getQuestion()));
        questionnaireDao.getAllByConsignmentId(consignmentId).forEach(questionnaire -> questionaries.add(new Questionary(questionnaire.getId(),questionMap.get(questionnaire.getQuestionId()),questionnaire.getResponse())));
        return questionaries;
    }
    public void newInvoice(Long consignmentId){
        settingDao.refresh();
        List<Invoice> invoices = invoiceDao.getAllByConsignmentId(consignmentId);
        for(Invoice invoice : invoices){
            if(!invoice.getStatus().equals("INACTIVE"))
                throw new AppException("Please make older invoices inactive before generating new invoice",HttpStatus.BAD_REQUEST);
        }
        Invoice invoice = new Invoice();
        invoice.setConsignmentId(consignmentId);
        if(invoices.size() == 0){
            Consignment consignment = consignmentDao.getById(consignmentId);
            invoice.setBillAddress(consignment.getMovingFrom());
            invoice.setShipAddress(consignment.getMovingTo());
        }
        else{
            Integer pos = invoices.size()-1;
            invoice.setBillAddress(invoices.get(pos).getBillAddress());
            invoice.setShipAddress(invoices.get(pos).getShipAddress());
        }
        Calendar now = Calendar.getInstance();
        invoice.setDate(new SimpleDateFormat(settingDao.get("DateFormat")).format(now.getTime()));
        now.add(Calendar.DATE, 7);
        invoice.setDueDate(new SimpleDateFormat(settingDao.get("DateFormat")).format(now.getTime()));
        invoice.setStatus("ACTIVE");
        invoiceDao.save(invoice);
    }
    public void makeRefund(Long consignmentId,String refundDesc,Float refundAmount){
        settingDao.refresh();
        Consignment consignment = consignmentDao.getById(consignmentId);
        if(consignment == null)
            throw new AppException("Consignment not found",HttpStatus.BAD_REQUEST);
        User user = userDao.getById(consignment.getUserId());
        if(user == null)
            throw new AppException("YUser not found",HttpStatus.BAD_REQUEST);
        Invoice invoice = invoiceDao.getByConsignmentId(consignmentId);
        if(invoice == null)
            throw new AppException("You cannot make refund without an Invoice",HttpStatus.BAD_REQUEST);
        InvoiceRecord invoiceRecord = new InvoiceRecord();
        invoiceRecord.setInvoiceId(invoice.getId());
        invoiceRecord.setDescription(refundDesc);
        invoiceRecord.setQuantity(-1);
        invoiceRecord.setUnitPrice(refundAmount);
        saveInvoiceRecord(invoiceRecord);
        logger.info("Action : makeRefund | Data : ConsignmentId = "+consignmentId+", Refund = "+settingDao.get("Currency")+" "+refundAmount);
        Mail mail = new Mail();
        mail.setTo(user.getEmail());
        mail.setCc("amit.shukla@amissible.com");
        mail.setBcc("anwar.mehmood@amissible.com");
        mail.setSubject("Hi! There is a refund on your Consignment");
        mail.setBody("AdminRefund.ftl");
        Map<String,String> map = new HashMap<>();
        map.put("name", user.getName());
        map.put("consignmentId", consignmentId.toString());
        map.put("refundAmount", settingDao.get("Currency")+" "+refundAmount.toString());
        map.put("refundDesc", refundDesc);
        mail.setModel(map);
        try{
            publisher.setMailBlockingQueue(mail);
        }
        catch (Exception e){
            logger.error("FAILED TO SEND REFUND : CONSIGNMENT => "+consignmentId+" ERROR => "+e.getMessage());
        }
    }
    public void deleteConsignment(Long consignmentId){
        if(paymentDao.getAllByConsignmentId(consignmentId).size() > 0)
            throw new AppException("There is a payment done on this consignment",HttpStatus.NOT_ACCEPTABLE);
        taskDao.deleteByConsignmentId(consignmentId);
        questionnaireDao.deleteByConsignmentId(consignmentId);
        Invoice invoice = invoiceDao.getByConsignmentId(consignmentId);
        if(invoice != null)
        {
            invoiceRecordDao.deleteByInvoiceId(invoice.getId());
            invoiceDao.deleteByConsignmentId(consignmentId);
        }
        feedbackDao.deleteByConsignmentId(consignmentId);
        complaintDao.deleteByConsignmentId(consignmentId);
        contractDao.deleteByConsignmentId(consignmentId);
        consignmentDao.delete(consignmentId);
    }



    public List<Employee> getEmployees(){
        return employeeDao.getAllByRole("EMPLOYEE");
    }
    public List<Task> getTaskData() {
        return taskDao.getAll();
    }
    public void updateTask(Task task){
        settingDao.refresh();
        Task task1 = taskDao.getById(task.getId());
        if(task1 == null)
            throw new AppException("Task not found",HttpStatus.NOT_FOUND);
        if(!task1.getStatus().equals("CLOSED") && task.getStatus().equals("CLOSED"))
            task.setClosedOn(new SimpleDateFormat(settingDao.get("DateTimeFormat")).format(new Date()));
        taskDao.update(task);
    }
    public void saveTask(Task task){
        settingDao.refresh();
        task.setIssuedOn(new SimpleDateFormat(settingDao.get("DateTimeFormat")).format(new Date()));
        taskDao.save(task);
    }
    public void deleteTask(Long taskId){
        taskDao.delete(taskId);
    }
    public List<Task> getTodayTask(String jwt){
        Employee employee = getEmployee(jwt);
        String date = new SimpleDateFormat(settingDao.get("DateFormat")).format(new Date());
        return taskDao.getByDatedTo(employee.getId(),date);
    }
    public List<Task> getPendingTask(String jwt){
        Employee employee = getEmployee(jwt);
        return taskDao.getPendingByEmployeeId(employee.getId());
    }
    public void pushData(Long taskId,String data){
        Task task = taskDao.getById(taskId);
        if(task == null)
            throw new AppException("Task not found",HttpStatus.NOT_FOUND);
        Consignment consignment = consignmentDao.getById(task.getConsignmentId());
        if(consignment == null)
            throw new AppException("Consignment not found",HttpStatus.NOT_FOUND);
        User user = userDao.getById(task.getUserId());
        if(user == null)
            throw new AppException("User not found",HttpStatus.NOT_FOUND);
        Employee employee = employeeDao.getById(task.getEmployeeId());
        if(employee == null)
            throw new AppException("Employee not found",HttpStatus.NOT_FOUND);
        consignment.setStatusLog(consignment.getStatusLog()+"\n"+employee.getName()+" : "+data);
        consignmentDao.update(consignment);
        Mail mail = new Mail();
        mail.setTo(user.getEmail());
        mail.setCc("amit.shukla@amissible.com");
        mail.setBcc("anwar.mehmood@amissible.com");
        mail.setSubject("Hi! There is an update on your Consignment");
        mail.setBody("AdminConsignmentUpdate.ftl");
        Map<String,String> map = new HashMap<>();
        map.put("name", user.getName());
        map.put("empName", employee.getName());
        map.put("update", data);
        mail.setModel(map);
        try{
            publisher.setMailBlockingQueue(mail);
        }
        catch (Exception e){
            logger.error("FAILED TO SEND STATUS_LOG EMAIL : TASK => "+task.toString()+" ERROR => "+e.getMessage());
        }
    }

    public Employee getEmployee(String jwt){
        settingDao.refresh();
        //String jwt = jwtFilter.resolveToken(httpServletRequest);
        Claims claims = tokenProvider.getClaims(jwt);
        Employee employee = employeeDao.getByEmail(claims.getSubject());
        employee.setPassword(settingDao.get("Currency"));
        logger.info("Action : getEmployee | Data : "+employee.toString());
        return employee;
    }

    public String getCurrency(){
        settingDao.refresh();
        return settingDao.get("Currency");
    }









    public void setDemoData(){
        for(long i=1;i<=1000;i++)
            complaintDao.save(new Complaint(i,"It is summary "+i,"Here goes details "+i,"","OPEN","2021-02-"+(i%25 +1)+" 10:20:30"));
    }

    public void test(){
        settingDao.refresh();
        Calendar now = Calendar.getInstance();
        Integer nodes[] = {0,1,1,3,5}; // 0,1,2,5,10
        Integer n = 0;
        String date = "";
        List<Consignment> consignments = consignmentDao.getAllByStatus("CONFIRMED");
        for(Integer node:nodes){
            n = n + node;
            now.add(Calendar.DATE, node);
            date = new SimpleDateFormat(settingDao.get("DateFormat")).format(now.getTime());
            for(Consignment consignment : consignments){
                if(consignment.getMovingDate().equals(date)){
                    User user = userDao.getById(consignment.getUserId());
                    Mail mail = new Mail();
                    mail.setTo(user.getEmail());
                    mail.setCc("amit.shukla@amissible.com");
                    mail.setBcc("anwar.mehmood@amissible.com");
                    if(node == 0){
                        mail.setSubject("Hi! Today is your movement");
                        mail.setBody("TodayIsYourMovenemt.ftl");
                    }
                    else {
                        mail.setSubject("Hi! Your movement is in "+n+" days");
                        mail.setBody("InNdaysYouHaveAMovement.ftl");
                    }
                    Map<String,String> map = new HashMap<>();
                    map.put("name", user.getName());
                    mail.setModel(map);
                    try{
                        publisher.setMailBlockingQueue(mail);
                    }
                    catch (Exception e){
                        logger.error("FAILED TO SEND NOTIFICATION EMAIL : ERROR => "+e.getMessage());
                    }
                }
            }
        }
    }
}
