package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.config.email.Publisher;
import com.amissible.movingsolutions.dao.ConsignmentDao;
import com.amissible.movingsolutions.dao.SettingDao;
import com.amissible.movingsolutions.dao.UserDao;
import com.amissible.movingsolutions.model.Consignment;
import com.amissible.movingsolutions.model.Mail;
import com.amissible.movingsolutions.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NotificationService {
    @Autowired
    ConsignmentDao consignmentDao;

    @Autowired
    UserDao userDao;

    @Autowired
    Publisher publisher;

    @Autowired
    SettingDao settingDao;

    List<Consignment> consignmentList;

    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

    //to be called once consignment status is changed from admin side
    void approveConsignment(Long consignmentId) throws InterruptedException {
        User user = userDao.getById(consignmentDao.getUserId(consignmentId));
        Mail mail = new Mail();
        mail.setTo(user.getEmail());
        mail.setCc("amit.shukla@amissible.com,anwar.mehmood@amissible.com");
        mail.setBcc("omendra.sgakkar@amissible.com,urvashi.dubey@amissible.com,sushmita.singh@amissible.com," +
                "sparsh.mishra@amissible.com");
        mail.setSubject("Consignment approved!");
        mail.setBody("Hello " + user.getName() + ",\n" + " Your consignment has been approved, " +
                "our employee will soon be in touch regarding the details of the consignment and user approval.");
        publisher.setMailBlockingQueue(mail);
    }

//    @Scheduled(cron = "0 * * * * *")
    void checkApprovedConsignments() throws InterruptedException {
        System.out.println("Checking consignments!");
        consignmentList = consignmentDao.getAll();
        for(Consignment itr : consignmentList) {
            if(itr.getStatus().equalsIgnoreCase("approved")) {
                User user = userDao.getById(consignmentDao.getUserId(itr.getId()));
                Mail mail = new Mail();
                mail.setTo(user.getEmail());
                mail.setCc("amit.shukla@amissible.com,anwar.mehmood@amissible.com");
                mail.setBcc("omendra.sgakkar@amissible.com,urvashi.dubey@amissible.com,sushmita.singh@amissible.com," +
                        "sparsh.mishra@amissible.com");
                mail.setSubject("Consignment approved!");
                mail.setBody("Hello " + user.getName() + ",\n" + " Your consignment has been approved, " +
                        "our employee will soon be in touch regarding the details of the consignment and user approval.");
                publisher.setMailBlockingQueue(mail);
            }
        }
    }

    @Scheduled(cron = "0 29 3 * * *")
    public void test(){
        logger.info("Running consignment job...");
        settingDao.refresh();
        Calendar now = Calendar.getInstance();
        Integer nodes[] = {0,1,1,3,5}; // 0,1,2,5,10
        Integer n = 0;
        String date = "";
        List<Consignment> consignments = consignmentDao.getAllByStatus("CONFIRMED");
        logger.info("Found " + consignments.size() + " with status CONFIRMED.");
        for(Integer node:nodes){
            n = n + node;
            now.add(Calendar.DATE, node);
            date = new SimpleDateFormat(settingDao.get("DateFormat")).format(now.getTime());
            for(Consignment consignment : consignments){
                if(consignment.getMovingDate().equals(date)){
                    User user = userDao.getById(consignment.getUserId());
                    Mail mail = new Mail();
                    mail.setTo(user.getEmail());
                    mail.setCc("amit.shukla@amissible.com");
                    mail.setBcc("anwar.mehmood@amissible.com,sparsh.mishra@amissible.com");
                    String days = "";
                    mail.setBody("InNdaysYouHaveAMovement.ftl");
                    if(node == 0){
                        mail.setSubject("Hi! Today is your movement");
                        days = "today";
                    }
                    else {
                        mail.setSubject("Hi! Your movement is in "+n+" days");
                        days = "in " + n.toString() + " days";
                    }
                    Map<String,String> map = new HashMap<>();
                    map.put("name", user.getName());
                    map.put("days", days);
                    mail.setModel(map);
                    logger.info("Attempting to notify users...");
                    try{
                        publisher.setMailBlockingQueue(mail);
                    }
                    catch (Exception e){
                        logger.error("FAILED TO SEND NOTIFICATION EMAIL : ERROR => "+e.getMessage());
                    }
                }
            }
        }
    }
}
