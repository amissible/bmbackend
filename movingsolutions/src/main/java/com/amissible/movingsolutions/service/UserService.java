package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.config.email.Publisher;
import com.amissible.movingsolutions.config.jwt.TokenProvider;
import com.amissible.movingsolutions.dao.UserDao;
import com.amissible.movingsolutions.model.Mail;
import com.amissible.movingsolutions.model.User;
import com.amissible.movingsolutions.model.UserPayload;
import com.amissible.movingsolutions.util.Utility;
import io.jsonwebtoken.Claims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserDao userDao;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    Publisher publisher;

    @Autowired
    TokenProvider tokenProvider;

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    public User getUserFromToken(String jwt) {
        Claims claims = tokenProvider.getClaims(jwt);
        return userDao.getByEmail(claims.getSubject());
    }

    public Long getUserId(String jwt) {
        User user = getUserFromToken(jwt);
        return user.getId();
    }

    public void resetPassword(String email) throws InterruptedException {
        User user = userDao.getByEmail(email);
        String password = Utility.generateNumericOTP(6);
        user.setPassword(encoder.encode(password)); // Use Encryptor

        //send mail regarding new password
        Mail mail = new Mail();
        mail.setTo(user.getEmail());
        mail.setCc("amit.shukla@amissible.com,anwar.mehmood@amissible.com");
        mail.setBcc("omendra.sgakkar@amissible.com,urvashi.dubey@amissible.com,sushmita.singh@amissible.com," +
                "sparsh.mishra@amissible.com");
        mail.setSubject("Password Reset");
        mail.setBody("Your password has been reset to " + password + " , login into your account to change it from user dashboard.");
        publisher.setMailBlockingQueue(mail);
        logger.info("Reset password requested for user: " + user.getEmail());
        userDao.update(user);
    }

    public ResponseEntity<?> changePassword(List<UserPayload> userPayload) {
        UserPayload oldUserPayload = userPayload.get(0);
        UserPayload newUserPayload = userPayload.get(1);
        //if emails don't match
        if(!oldUserPayload.getEmail().equalsIgnoreCase(newUserPayload.getEmail())) {
            logger.warn("Attempting to modify password of invalid email!");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        //get user  by email
        User currUser = userDao.getByEmail(oldUserPayload.getEmail());
        if(currUser == null) {
            logger.warn("Attempt to modify a password for a non-existing user!");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else if(encoder.matches(oldUserPayload.getPassword(), currUser.getPassword())) {
            currUser.setPassword(encoder.encode(newUserPayload.getPassword()));
            userDao.update(currUser);
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
