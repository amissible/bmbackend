package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.dao.EmployeeDao;
import com.amissible.movingsolutions.dao.UserDao;
import com.amissible.movingsolutions.model.Employee;
import com.amissible.movingsolutions.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class JwtUserDetailsService implements UserDetailsService {


    private static final Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class);

    @Autowired
    UserDao userDao;

    @Autowired
    EmployeeDao employeeDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.getByEmail(username);
        Employee employee = employeeDao.getByEmail(username);
        if (user == null && employee == null) {
            logger.warn("No user found with email: " + username);
            System.out.println("null");
            throw new UsernameNotFoundException("No user with email " + username +
                    " exists");
        }  else if(employee != null) { //employee
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(employee.getRole()));
            UserDetails springUser = new org.springframework.security.core.userdetails.User(employee.getEmail(), employee.getPassword(),
                    authorities);
            return springUser;
        } else { //customer
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("USER"));
            UserDetails springUser = new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                    authorities);
            return springUser;
        }
    }
}