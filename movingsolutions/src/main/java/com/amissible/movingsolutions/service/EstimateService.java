package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.config.email.Publisher;
import com.amissible.movingsolutions.dao.*;
import com.amissible.movingsolutions.model.Consignment;
import com.amissible.movingsolutions.model.Mail;
import com.amissible.movingsolutions.model.User;
import com.amissible.movingsolutions.payload.Estimate;
import com.amissible.movingsolutions.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EstimateService {

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ConsignmentDao consignmentDao;

    @Autowired
    private QuestionDao questionDao;

    @Autowired
    private QuestionnaireDao questionnaireDao;

    @Autowired
    private PaymentDao paymentDao;

    @Autowired
    private InvoiceDao invoiceDao;

    @Autowired
    private InvoiceRecordDao invoiceRecordDao;

    @Autowired
    private ContractDao contractDao;

    @Autowired
    private ComplaintDao complaintDao;

    @Autowired
    private FeedbackDao feedbackDao;

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private EmployeeTaskDao employeeTaskDao;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    Publisher publisher;

    private static final Logger logger = LoggerFactory.getLogger(EstimateService.class);

    public SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public Long sendEstimateByEmail(@RequestBody Estimate estimate) throws InterruptedException {
        User user = userDao.getByEmail(estimate.getEmail());
        String password = null;
        if(user == null){
            user = new User();
            user.setName(estimate.getName());
            user.setEmail(estimate.getEmail());
            user.setContact(estimate.getContact());
            password = Utility.generateNumericOTP(6);
            user.setPassword(encoder.encode(password)); // Use Encryptor
            user.setTimeZone(estimate.getTimeZone());
            user.setTimeStamp(simpleDateFormat.format(new Date()));
            userDao.save(user);
            user = userDao.getByEmail(estimate.getEmail());
        }

        logger.info("Attempting to send an email.");
        // Send Email with password
        Mail mail = new Mail();
        mail.setTo(user.getEmail());
        mail.setCc("amit.shukla@amissible.com,anwar.mehmood@amissible.com");
        mail.setBcc("omendra.sgakkar@amissible.com,urvashi.dubey@amissible.com,sushmita.singh@amissible.com," +
                "sparsh.mishra@amissible.com");
        mail.setSubject("Processing request");
        mail.setBody("EmailTemplate.ftl");
        Map<String,String> map = new HashMap<>();
        map.put("name", user.getName());
        map.put("email", user.getEmail());
        if(password != null)
            map.put("password", password);
        else
            map.put("password", user.getPassword());
        mail.setModel(map);

        publisher.setMailBlockingQueue(mail);

        Consignment consignment = new Consignment();
        consignment.setUserId(user.getId());
        consignment.setMovingFromZip(estimate.getMovingFromZip());
        consignment.setMovingToZip(estimate.getMovingToZip());
        consignment.setServiceType(estimate.getServiceType());
        consignment.setMovingDate(estimate.getMovingDate());
        String timeStamp = simpleDateFormat.format(new Date());
        consignment.setTimeStamp(timeStamp);
        consignment.setStatus("NEW");

        consignmentDao.save(consignment);
        List<Consignment> consignments = consignmentDao.getAllByUserId(user.getId());
        for(Consignment c : consignments){
            if(c.getTimeStamp().equals(timeStamp))
                consignment = c;
        }
        logger.info("Saved consignment for user " + user.getEmail());
        return consignment.getId();
    }
}