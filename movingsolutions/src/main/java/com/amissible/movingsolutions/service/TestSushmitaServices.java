package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.dao.*;
import com.amissible.movingsolutions.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Service
public class TestSushmitaServices {

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ConsignmentDao consignmentDao;

    @Autowired
    private QuestionDao questionDao;

    @Autowired
    private QuestionnaireDao questionnaireDao;

    @Autowired
    private PaymentDao paymentDao;

    @Autowired
    private InvoiceDao invoiceDao;

    @Autowired
    private InvoiceRecordDao invoiceRecordDao;

    @Autowired
    private ContractDao contractDao;

    @Autowired
    private ComplaintDao complaintDao;

    @Autowired
    private FeedbackDao feedbackDao;

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private EmployeeTaskDao employeeTaskDao;

    private static final Logger logger = LoggerFactory.getLogger(TestSushmitaServices.class);

    public String testComplaint(){
        //complaintDao.createTable();
        Complaint complaint1 = new Complaint(1025l,"Consignment 1"," saved", " 89 X 90"," Moved", "18:39");
        complaintDao.save(complaint1);
        Complaint complaint2 = new Complaint(1020l,"Consignment 7"," saved", " 40 X 90","To be Moved", "18:39");
        complaintDao.save(complaint2);
        System.out.println(complaintDao.getAll()+"\n");
        System.out.println();
        System.out.println("get by status  "+complaintDao.getAllByStatus(" Moved"));
        System.out.println();
        System.out.println("get by timestamp  "+complaintDao.getAllByTimeStamp("18:39"));
        System.out.println();
        System.out.println("get by id  "+complaintDao.getById(1l));
        System.out.println();
        System.out.println("get by consignment id  "+complaintDao.getByConsignmentId(1025l));
        Complaint update = new Complaint(1l,1025l,"Consignment of home","Home ","90 X 90","Moved","20:30");
        System.out.println(update);
        complaintDao.update(update);
        return "CHECKED";
    }

    public String testContract(){
        contractDao.createTable();
        Contract contract1 = new Contract(1025l, "15-02-2021", "Declared", "first party", "second party", "Terms & Conditions", "url", "signedurl");
        contractDao.save(contract1);
        Contract contract2 = new Contract(1020l, "19-02-2021", "Declared", "first party", "second party", "Terms & Conditions", "url", "signedurl");
        contractDao.save(contract2);
        System.out.println("get all :\n" + contractDao.getAll());
        System.out.println("get all by date :\n" + contractDao.getAllByDate("19-02-2021"));
        System.out.println("get all by id :\n" + contractDao.getById(1l));
        System.out.println("get all by consignment id:\n" + contractDao.getByConsignmentId(1025l));
        Contract update = new Contract(1l,1025l, "14-02-2021", "Declared", "first party", "second party", "Terms & Conditions", "url", "signedurl");
        contractDao.update(update);
        return " CHECKED ";
    }

    public String testEmployee(){
        employeeDao.createTable();
        Employee employee1 = new Employee("James", "james@gmail.com", "989-056-7486", "james@123", "Driver", "18:49");
        employeeDao.save(employee1);
        Employee employee2 = new Employee("Jimmy", "jimmy@gmail.com", "700-056-7556", "jimmy@123", "worker", "18:49");
        employeeDao.save(employee2);
        System.out.println("get all :\n "+ employeeDao.getAll());
        System.out.println("get all by role :\n "+ employeeDao.getAllByRole("Driver"));
        System.out.println("get all timestamp :\n "+ employeeDao.getAllByTimeStamp("18:49"));
        System.out.println("get by id :\n "+ employeeDao.getById(2l));
        System.out.println("get all email :\n "+ employeeDao.getByEmail("jimmy@gmail.com"));
        Employee update = new Employee(1l,"James","james@yahoo","100-700-8456","james@123","Driver","18:50");
        employeeDao.update(update);
        return "CHECKED ";
    }

    public String testEmployeeTask(){
        employeeTaskDao.createTable();
        EmployeeTask employeeTask1 = new EmployeeTask(1l, 1025l, "Moved");
        EmployeeTask employeeTask2 = new EmployeeTask(2l, 1020l, "Packed");
        employeeTaskDao.save(employeeTask1);
        employeeTaskDao.save(employeeTask2);
        System.out.println("get all :\n" + employeeTaskDao.getAll());
        System.out.println("get by employee id :\n" + employeeTaskDao.getAllByEmployeeId(1l));
        System.out.println("get all by consignment id:\n" + employeeTaskDao.getAllByConsignmentId(1020l));
        System.out.println("get all by status :\n" + employeeTaskDao.getAllByStatus("Moved"));
        System.out.println("get all by employeeId & consignmentId:\n" + employeeTaskDao.getAllByEC(1l,1025l));
        System.out.println("get all by employeeId & status:\n" + employeeTaskDao.getAllByCS(1025l,"Moved"));
        System.out.println("get all by employeeId & consignmentId & status :\n" + employeeTaskDao.getAllByECS(1l,1025l,"Packed"));
        EmployeeTask update = new EmployeeTask(1l,1l,1025l,"Moving");
        return "CHECKED ";
    }

    public String testFeedback(){
       // feedbackDao.createTable();
        Feedback feedback1 = new Feedback(1025l, 5, "Good", "17:29");
        Feedback feedback2 = new Feedback(1020l, 3, "Average", "19:09");
        feedbackDao.save(feedback1);
        feedbackDao.save(feedback2);
        System.out.println("feedbacks are : \n "+ feedbackDao.getAll());
       System.out.println("get by timestamp : \n "+ feedbackDao.getAllByTimeStamp("19:29"));
       System.out.println("get by id : \n "+ feedbackDao.getById(2l));
       System.out.println("get by consignment id : \n "+ feedbackDao.getByConsignmentId(1025l));
       Feedback update = new Feedback(1l,1025l,4,"Good ","18:50");
       feedbackDao.update(update);
       return "CHECKED ";
    }

    public String testInvoice(){
        invoiceDao.createTable();
        Invoice invoice = new Invoice(1025l, "bill address","ship address","17-02-2021","17-02-2021","Moved", 97000f, 0.0f,0.0f, 97000f);
        invoiceDao.save(invoice);
        System.out.println("get All \n"+invoiceDao.getAll());
        System.out.println("get All by date \n"+invoiceDao.getAllByDate("17-02-2021"));
        System.out.println("get All by due date\n"+invoiceDao.getAllByDueDate("16-02-2021"));
        System.out.println("get All by status \n"+invoiceDao.getAllByStatus("Moved"));
        System.out.println("get by id \n"+invoiceDao.getById(1l));
        Invoice update = new Invoice(1l,1025l, "bill address","ship address","18-02-2021","18-02-2021","Moved", 98000f, 0.0f,0.0f, 98000f);
        invoiceDao.update(update);
        return "CHECKED ";
    }
     /*
     */

    public String testInvoiceRecord(){
        invoiceRecordDao.createTable();
        InvoiceRecord invoiceRecord = new InvoiceRecord(1l, "Generated invoice 1 ", 403, 800f, 98000f);
        invoiceRecordDao.save(invoiceRecord);
        System.out.println("get all :\n"+invoiceRecordDao.getAll());
        System.out.println("get by invoice id :\n"+invoiceRecordDao.getAllByInvoiceId(1l));
        System.out.println("get by id:\n"+invoiceRecordDao.getById(1l));
        InvoiceRecord update = new InvoiceRecord(1l,1l, "Generated invoice 1 ", 400, 800f, 98000f);
        invoiceRecordDao.update(update);
        return "CHECKED ";
        /*
         */
    }

    public String testPayment(){
        paymentDao.createTable();
        Payment payment = new Payment(1l,1025l, "TXN1", 9000f, "Completed", "19:05");
        paymentDao.save(payment);
        System.out.println("get all :\n"+paymentDao.getAll());

        /*
    List<Payment> getAllByUserId(Long userId);
    List<Payment> getAllByConsignmentId(Long consignmentId);
    List<Payment> getAllByTxnStatus(String txnStatus);
    List<Payment> getAllByTxnTimeStamp(String txnTimeStamp);
    Payment getById(Long id);
    void update(Payment payment);
    void save(Payment payment);
         */
        return "CHECKED ";
    }

    public String testQuestion(){
        questionDao.createTable();
        return "CHECKED ";
    }

    public String testQuestionnaire(){
        questionnaireDao.createTable();
        return "CHECKED ";
    }

    public String testUser(){
        userDao.createTable();
        return "CHECKED ";
    }

}
