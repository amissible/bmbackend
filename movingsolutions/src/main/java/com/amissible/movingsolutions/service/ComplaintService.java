package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.dao.ComplaintDao;
import com.amissible.movingsolutions.model.Complaint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComplaintService {

    @Autowired
    ComplaintDao complaintDao;

    public String getComplaint(Long consignmentId, String summary, String details, String resolution, String status, String timeStamp){
        Complaint complaint = new Complaint(consignmentId,summary,details,resolution,status,timeStamp);
        complaintDao.save(complaint);
        return "COMPLAINT SUBMITTED ";
    }
}
