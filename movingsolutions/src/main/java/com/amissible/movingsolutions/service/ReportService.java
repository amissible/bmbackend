package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.dao.UserDao;
import com.amissible.movingsolutions.model.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class ReportService {

    @Autowired
    UserDao userDao;

    private final String importPath = "/app/import/";

    private static final Logger logger = LoggerFactory.getLogger(ReportService.class);

    public String exportInvoice(String reportFormat, InvoicePayload invoicePayload, String path) throws FileNotFoundException, JRException {
        //Temp company details
        CompanyDetails companyDetails = new CompanyDetails(invoicePayload.getId(), "src/main/resources/images/logo.png", "Bill and Movers", "Bill And Movers", 1010111L, "BnM@Movers.com", "BillMovers.com");

        //load file and compile it
        File file = new File(importPath + "Invoice.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Collections.singletonList("Invoice"));
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("path", companyDetails.getLogoUrl()); //static path for temporary usage
        parameters.put("companyDetails", companyDetails);
        parameters.put("invoice", invoicePayload);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "/invoice/invoice_" + invoicePayload.getId() + ".html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/invoice/invoice_" + invoicePayload.getId() + ".pdf");
        }
        logger.info("Generated invoice.");
        return path + "/invoice/";
    }

    public String exportPayment(String reportFormat, Payment payment, String path) throws FileNotFoundException, JRException {
       User user = userDao.getById(payment.getUserId());
        UserPayload userPayload = new UserPayload(user.getEmail(), user.getPassword());
        //Temp company details
        CompanyDetails companyDetails = new CompanyDetails(payment.getId(), "src/main/resources/images/logo.png", "Bill and Movers",
                "Bill And Movers", 1010111L, "BnM@Movers.com", "BillMovers.com");

        //load file and compile it
        File file = new File(importPath + "Payment.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Collections.singletonList("Invoice"));
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("userPayload", userPayload);
        parameters.put("user", user); //static path for temporary usage
        parameters.put("companyDetails", companyDetails);
        parameters.put("payment", payment);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "/payment/payment_" + payment.getId() + ".html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/payment/payment_" + payment.getId() + ".pdf");
        }
        logger.info("Generated payment receipt.");
        return path + "/payment/";
    }

    public String exportContract(String reportFormat, Contract contract, String path) throws FileNotFoundException, JRException {
        //Temp company details
        CompanyDetails companyDetails = new CompanyDetails(contract.getId(), "src/main/resources/images/logo.png", "Bill and Movers",
                "Bill And Movers", 1010111L, "BnM@Movers.com", "BillMovers.com");

        //load file and compile it
        File file = new File(importPath + "Contract.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Collections.singletonList("Invoice"));
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("companyDetails", companyDetails);
        parameters.put("contract", contract);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        if (reportFormat.equalsIgnoreCase("html")) {
            JasperExportManager.exportReportToHtmlFile(jasperPrint, path + "/contract/contract_" + contract.getId() + ".html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")) {
            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "/contract/contract_" + contract.getId() + ".pdf");
        }
        logger.info("Generated unsigned contract.");
        return path + "/contract/";
    }

    public String signContract(Contract contract, String path) throws FileNotFoundException, JRException {
        //Temp company details
        CompanyDetails companyDetails = new CompanyDetails(contract.getId(), "src/main/resources/images/logo.png", "Bill and Movers",
                "Bill And Movers", 1010111L, "BnM@Movers.com", "BillMovers.com");

        //load file and compile it
        File file = new File(importPath + "SignedContract.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Collections.singletonList("Invoice"));
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("companyDetails", companyDetails);
        parameters.put("contract", contract);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint,  path + "/contract/signed_contract_" + contract.getId() + ".pdf");
        logger.info("Generated signed contract.");
        return path + "/contract";
    }
}