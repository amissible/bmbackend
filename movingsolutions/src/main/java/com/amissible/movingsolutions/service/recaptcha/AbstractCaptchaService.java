package com.amissible.movingsolutions.service.recaptcha;

import com.amissible.movingsolutions.config.CaptchaConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

public class AbstractCaptchaService implements ICaptchaService {
    private final static Logger LOGGER = LoggerFactory.getLogger(AbstractCaptchaService.class);

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected CaptchaConfig captchaSettings;

    @Autowired
    protected ReCaptchaAttemptService reCaptchaAttemptService;

    protected static final Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");

    protected static final String RECAPTCHA_URL_TEMPLATE = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s";

    @Override
    public String getReCaptchaSite() {
        return captchaSettings.getSite();
    }

    @Override
    public String getReCaptchaSecret() {
        return captchaSettings.getSecret();
    }


    protected void securityCheck(final String response) throws Exception {
        LOGGER.info("Attempting to validate response {}", response);

        if (reCaptchaAttemptService.isBlocked(getClientIP())) {
            throw new Exception("Client exceeded maximum number of failed attempts");
        }

        if (!responseSanityCheck(response)) {
            throw new Exception("Response contains invalid characters");
        }
    }

    protected boolean responseSanityCheck(final String response) {
        return StringUtils.hasLength(response) && RESPONSE_PATTERN.matcher(response).matches();
    }

    protected String getClientIP() {
        final String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null) {
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }
}
