package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.dao.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Services { // Add tested and verified code only

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ConsignmentDao consignmentDao;

    @Autowired
    private QuestionDao questionDao;

    @Autowired
    private QuestionnaireDao questionnaireDao;

    @Autowired
    private PaymentDao paymentDao;

    @Autowired
    private InvoiceDao invoiceDao;

    @Autowired
    private InvoiceRecordDao invoiceRecordDao;

    @Autowired
    private ContractDao contractDao;

    @Autowired
    private ComplaintDao complaintDao;

    @Autowired
    private FeedbackDao feedbackDao;

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private EmployeeTaskDao employeeTaskDao;

    private static final Logger logger = LoggerFactory.getLogger(Services.class);

    public void setup(){
        userDao.createTable();
        consignmentDao.createTable();
        questionDao.createTable();
        questionnaireDao.createTable();
        paymentDao.createTable();
        invoiceDao.createTable();
        invoiceRecordDao.createTable();
        contractDao.createTable();
        complaintDao.createTable();
        feedbackDao.createTable();
        employeeDao.createTable();
        employeeTaskDao.createTable();
    }

}
