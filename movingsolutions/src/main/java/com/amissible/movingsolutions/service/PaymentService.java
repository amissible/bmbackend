package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.dao.PaymentDao;
import com.amissible.movingsolutions.dao.UserDao;
import com.amissible.movingsolutions.model.Payment;
import com.amissible.movingsolutions.model.User;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.lowagie.text.pdf.*;
import com.lowagie.text.*;


@Service
public class PaymentService {

    @Autowired
    PaymentDao paymentDao;

    public List<Payment> getPaymentListByConsignment(Long consignmentId) {
        return paymentDao.getAllByConsignmentId(consignmentId);
    }

    public List<Payment> getPaymentListByUserId(Long userId) {
        return paymentDao.getAllByUserId(userId);
    }

    public Payment getPayment(Long paymentId) {
        return paymentDao.getById(paymentId);
    }
}