package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.dao.ComplaintDao;
import com.amissible.movingsolutions.dao.FeedbackDao;
import com.amissible.movingsolutions.model.Complaint;
import com.amissible.movingsolutions.model.Feedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FeedbackService {

    @Autowired
    private FeedbackDao feedbackDao;

    @Autowired
    private ComplaintDao complaintDao;

    public void saveFeedback(Feedback feedback){
        feedbackDao.save(feedback);
    }
    public Feedback getFeedback(Long consignmentId){
        return feedbackDao.getByConsignmentId(consignmentId);
    }

    public void saveComplaint(Complaint complaint){
        complaintDao.save(complaint);
    }
    public Complaint getComplaint(Long consignmentId){
        return complaintDao.getByConsignmentId(consignmentId);
    }
}
