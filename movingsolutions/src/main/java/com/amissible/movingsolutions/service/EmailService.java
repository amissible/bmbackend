package com.amissible.movingsolutions.service;

import com.amissible.movingsolutions.exception.AppException;
import com.amissible.movingsolutions.model.Mail;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.nio.charset.StandardCharsets;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private Configuration freemarkerConfig;

    @Value("${spring.mail.username}")
    String from;

    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    public void sendSimpleMessage(Mail mail){
        try{
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());
            String attachments[] = mail.getAttachments();
            if(attachments != null){
                for(String path : attachments){
                    File file = new File(path);
                    helper.addAttachment(file.getName(),file);
                }
            }
            //helper.addAttachment("logo.jpg", new ClassPathResource("/logo.jpg"));
            //helper.addAttachment("logo.jpg",new File("/logo.jpg"));

            //template is nullable
            String html = null;
            Template t = null;

            //we will query if body field has a template, returns null if string is not found
            try {
                logger.info("Attempting to open template: " + mail.getBody());
                t = freemarkerConfig.getTemplate(mail.getBody());
                html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail.getModel());
            } catch (Exception e) {
                logger.error(e.getMessage());
            }


            helper.setTo(mail.getTo());
            if(mail.getCc() != null)
                helper.setCc(mail.getCc().split(","));
            if(mail.getBcc() != null)
                helper.setBcc(mail.getBcc().split(","));
            helper.setSubject(mail.getSubject());
            helper.setFrom(from);

            if(t == null)
                helper.setText(mail.getBody());
            else
                helper.setText(html, true);

            emailSender.send(message);
            logger.info("Sent an email.");
        }
        catch (Exception e){
            throw new AppException("EMAIL_SERVICE_ERROR : "+e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}