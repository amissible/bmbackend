package com.amissible.movingsolutions.model;

public class Task {
    private Long id;
    private Long userId;
    private Long consignmentId;
    private Long employeeId;
    private String instructions;
    private String notes;
    private String status;
    private String issuedOn;
    private String datedTo;
    private String closedOn;

    public Task() {
    }

    public Task(Long userId, Long consignmentId, Long employeeId, String instructions, String notes, String status, String issuedOn, String datedTo, String closedOn) {
        this.userId = userId;
        this.consignmentId = consignmentId;
        this.employeeId = employeeId;
        this.instructions = instructions;
        this.notes = notes;
        this.status = status;
        this.issuedOn = issuedOn;
        this.datedTo = datedTo;
        this.closedOn = closedOn;
    }

    public Task(Long id, Long userId, Long consignmentId, Long employeeId, String instructions, String notes, String status, String issuedOn, String datedTo, String closedOn) {
        this.id = id;
        this.userId = userId;
        this.consignmentId = consignmentId;
        this.employeeId = employeeId;
        this.instructions = instructions;
        this.notes = notes;
        this.status = status;
        this.issuedOn = issuedOn;
        this.datedTo = datedTo;
        this.closedOn = closedOn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(Long consignmentId) {
        this.consignmentId = consignmentId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIssuedOn() {
        return issuedOn;
    }

    public void setIssuedOn(String issuedOn) {
        this.issuedOn = issuedOn;
    }

    public String getDatedTo() {
        return datedTo;
    }

    public void setDatedTo(String datedTo) {
        this.datedTo = datedTo;
    }

    public String getClosedOn() {
        return closedOn;
    }

    public void setClosedOn(String closedOn) {
        this.closedOn = closedOn;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", userId=" + userId +
                ", consignmentId=" + consignmentId +
                ", employeeId=" + employeeId +
                ", instructions='" + instructions + '\'' +
                ", notes='" + notes + '\'' +
                ", status='" + status + '\'' +
                ", issuedOn='" + issuedOn + '\'' +
                ", datedTo='" + datedTo + '\'' +
                ", closedOn='" + closedOn + '\'' +
                '}';
    }
}
