package com.amissible.movingsolutions.model;

public class EmployeeTask {

    private Long id;
    private Long employeeId;
    private Long consignmentId;
    private String status;

    public EmployeeTask() {
        id = employeeId = consignmentId = 0L;
        status = "";
    }

    public EmployeeTask(Long employeeId, Long consignmentId, String status) {
        this.employeeId = employeeId;
        this.consignmentId = consignmentId;
        this.status = status;
    }

    public EmployeeTask(Long id, Long employeeId, Long consignmentId, String status) {
        this.id = id;
        this.employeeId = employeeId;
        this.consignmentId = consignmentId;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(Long consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "EmployeeTask{" +
                "id=" + id +
                ", employeeId=" + employeeId +
                ", consignmentId=" + consignmentId +
                ", status='" + status + '\'' +
                '}';
    }
}
