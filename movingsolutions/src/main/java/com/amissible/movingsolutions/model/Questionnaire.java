package com.amissible.movingsolutions.model;

public class Questionnaire {

    private Long id;
    private Long consignmentId;
    private Long questionId;
    private String response;

    public Questionnaire() {
        id = consignmentId = questionId = 0L;
        response = "";
    }

    public Questionnaire(Long consignmentId, Long questionId, String response) {
        this.consignmentId = consignmentId;
        this.questionId = questionId;
        this.response = response;
    }

    public Questionnaire(Long id, Long consignmentId, Long questionId, String response) {
        this.id = id;
        this.consignmentId = consignmentId;
        this.questionId = questionId;
        this.response = response;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(Long consignmentId) {
        this.consignmentId = consignmentId;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "Questionnaire{" +
                "id=" + id +
                ", consignmentId=" + consignmentId +
                ", questionId=" + questionId +
                ", response='" + response + '\'' +
                '}';
    }
}