package com.amissible.movingsolutions.model;

import java.util.Arrays;
import java.util.Map;

public class Mail {

    private String to;
    private String cc;
    private String bcc;
    private String subject;
    private String body;
    private String attachments[];
    Map<String,String> model;

    public Mail() {
    }

    public Mail(String to, String subject, String body) {
        this.to = to;
        this.subject = subject;
        this.body = body;
    }

    public Mail(String to, String cc, String bcc, String subject, String template) {
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.subject = subject;
        this.body = template;
    }

    public Mail(String to, String subject, String template, String[] attachments) {
        this.to = to;
        this.subject = subject;
        this.body = template;
        this.attachments = attachments;
    }

    public Mail(String from, String to, String cc, String bcc, String subject, String template, String[] attachments) {
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.subject = subject;
        this.body = template;
        this.attachments = attachments;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String[] getAttachments() {
        return attachments;
    }

    public void setAttachments(String[] attachments) {
        this.attachments = attachments;
    }

    public Map<String,String> getModel() {
        return model;
    }

    public void setModel(Map<String,String> model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "to='" + to + '\'' +
                ", cc='" + cc + '\'' +
                ", bcc='" + bcc + '\'' +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                ", attachments=" + Arrays.toString(attachments) +
                ", model=" + model +
                '}';
    }
}