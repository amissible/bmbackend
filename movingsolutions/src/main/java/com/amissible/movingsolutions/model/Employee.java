package com.amissible.movingsolutions.model;

public class Employee {

    private Long id;
    private String name;
    private String email;
    private String contact;
    private String password;
    private String role;
    private String timeStamp;

    public Employee() {
        id = 0L;
        name = email = contact = password = role = timeStamp = "";
    }

    public Employee(String name, String email, String contact, String password, String role, String timeStamp) {
        this.id = 0L;
        this.name = name;
        this.email = email;
        this.contact = contact;
        this.password = password;
        this.role = role;
        this.timeStamp = timeStamp;
    }

    public Employee(Long id, String name, String email, String contact, String password, String role, String timeStamp) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.contact = contact;
        this.password = password;
        this.role = role;
        this.timeStamp = timeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", contact='" + contact + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                '}';
    }

}
