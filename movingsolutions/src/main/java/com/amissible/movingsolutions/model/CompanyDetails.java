package com.amissible.movingsolutions.model;

public class CompanyDetails {
    private Long invoiceId;
    private String logoUrl;
    private String companyName;
    private String streetAddress;
    private Long phone;
    private String email;
    private String companyWebsite;

    public CompanyDetails() {
    }

    public CompanyDetails(Long invoiceId, String logoUrl,
                          String companyName, String streetAddress,
                          Long phone, String email, String companyWebsite) {
        this.invoiceId = invoiceId;
        this.logoUrl = logoUrl;
        this.companyName = companyName;
        this.streetAddress = streetAddress;
        this.phone = phone;
        this.email = email;
        this.companyWebsite = companyWebsite;
    }

    public CompanyDetails(String logoUrl, String companyName,
                          String streetAddress, Long phone,
                          String email, String companyWebsite) {
        this.logoUrl = logoUrl;
        this.companyName = companyName;
        this.streetAddress = streetAddress;
        this.phone = phone;
        this.email = email;
        this.companyWebsite = companyWebsite;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    @Override
    public String toString() {
        return "CompanyDetails{" +
                ", invoiceId='" + invoiceId + '\'' +
                ", logoUrl='" + logoUrl + '\'' +
                ", companyName='" + companyName + '\'' +
                ", streetAddress='" + streetAddress + '\'' +
                ", phone=" + phone +
                ", email='" + email + '\'' +
                ", companyWebsite='" + companyWebsite + '\'' +
                '}';
    }
}