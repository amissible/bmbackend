package com.amissible.movingsolutions.model;

public class Consignment {

    private Long id;
    private Long userId;
    private String movingFromZip;
    private String movingToZip;
    private String movingFrom;
    private String movingTo;
    private String movingDate;
    private String serviceType;
    private String timeStamp;
    private String status;
    private String statusLog;

    public Consignment() {
        id = userId = 0L;
        movingFromZip = movingToZip = movingFrom = movingTo = movingDate = serviceType = timeStamp = status = statusLog = "";
    }

    public Consignment(Long userId, String movingFromZip, String movingToZip, String movingFrom, String movingTo, String movingDate, String serviceType, String timeStamp, String status, String statusLog) {
        this.id = 0L;
        this.userId = userId;
        this.movingFromZip = movingFromZip;
        this.movingToZip = movingToZip;
        this.movingFrom = movingFrom;
        this.movingTo = movingTo;
        this.movingDate = movingDate;
        this.serviceType = serviceType;
        this.timeStamp = timeStamp;
        this.status = status;
        this.statusLog = statusLog;
    }

    public Consignment(Long id, Long userId, String movingFromZip, String movingToZip, String movingFrom, String movingTo, String movingDate, String serviceType, String timeStamp, String status, String statusLog) {
        this.id = id;
        this.userId = userId;
        this.movingFromZip = movingFromZip;
        this.movingToZip = movingToZip;
        this.movingFrom = movingFrom;
        this.movingTo = movingTo;
        this.movingDate = movingDate;
        this.serviceType = serviceType;
        this.timeStamp = timeStamp;
        this.status = status;
        this.statusLog = statusLog;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMovingFromZip() {
        return movingFromZip;
    }

    public void setMovingFromZip(String movingFromZip) {
        this.movingFromZip = movingFromZip;
    }

    public String getMovingToZip() {
        return movingToZip;
    }

    public void setMovingToZip(String movingToZip) {
        this.movingToZip = movingToZip;
    }

    public String getMovingFrom() {
        return movingFrom;
    }

    public void setMovingFrom(String movingFrom) {
        this.movingFrom = movingFrom;
    }

    public String getMovingTo() {
        return movingTo;
    }

    public void setMovingTo(String movingTo) {
        this.movingTo = movingTo;
    }

    public String getMovingDate() {
        return movingDate;
    }

    public void setMovingDate(String movingDate) {
        this.movingDate = movingDate;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusLog() {
        return statusLog;
    }

    public void setStatusLog(String statusLog) {
        this.statusLog = statusLog;
    }

    @Override
    public String toString() {
        return "Consignment{" +
                "id=" + id +
                ", userId=" + userId +
                ", movingFromZip='" + movingFromZip + '\'' +
                ", movingToZip='" + movingToZip + '\'' +
                ", movingFrom='" + movingFrom + '\'' +
                ", movingTo='" + movingTo + '\'' +
                ", movingDate='" + movingDate + '\'' +
                ", serviceType='" + serviceType + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                ", status='" + status + '\'' +
                ", statusLog='" + statusLog + '\'' +
                '}';
    }
}
