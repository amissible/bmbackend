package com.amissible.movingsolutions.model;

import java.util.List;

public class InvoicePayload {

    private Long id;
    private Long consignmentId;
    private String billAddress;
    private String shipAddress;
    private String date;
    private String dueDate;
    private String status;
    private Float subTotal;
    private Float discount;
    private Float tax;
    private Float total;
    List<InvoiceRecord> invoiceRecordList;

    public InvoicePayload() {
    }

    public InvoicePayload(Long id, Long consignmentId, String billAddress, String shipAddress, String date, String dueDate,
                          String status, Float subTotal, Float discount, Float tax, Float total, List<InvoiceRecord> invoiceRecordList) {
        this.id = id;
        this.consignmentId = consignmentId;
        this.billAddress = billAddress;
        this.shipAddress = shipAddress;
        this.date = date;
        this.dueDate = dueDate;
        this.status = status;
        this.subTotal = subTotal;
        this.discount = discount;
        this.tax = tax;
        this.total = total;
        this.invoiceRecordList = invoiceRecordList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(Long consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getBillAddress() {
        return billAddress;
    }

    public void setBillAddress(String billAddress) {
        this.billAddress = billAddress;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Float subTotal) {
        this.subTotal = subTotal;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public Float getTax() {
        return tax;
    }

    public void setTax(Float tax) {
        this.tax = tax;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public List<InvoiceRecord> getInvoiceRecordList() {
        return invoiceRecordList;
    }

    public void setInvoiceRecordList(List<InvoiceRecord> invoiceRecordList) {
        this.invoiceRecordList = invoiceRecordList;
    }

    @Override
    public String toString() {
        return "InvoicePayload{" +
                "id=" + id +
                ", consignmentId=" + consignmentId +
                ", billAddress='" + billAddress + '\'' +
                ", shipAddress='" + shipAddress + '\'' +
                ", date='" + date + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", status='" + status + '\'' +
                ", subTotal=" + subTotal +
                ", discount=" + discount +
                ", tax=" + tax +
                ", total=" + total +
                ", invoiceRecordList=" + invoiceRecordList +
                '}';
    }
}
