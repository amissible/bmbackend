package com.amissible.movingsolutions.model;

public class Feedback {

    private Long id;
    private Long consignmentId;
    private Integer rating;
    private String review;
    private String timeStamp;

    public Feedback() {
        id = consignmentId = 0L;
        rating = 0;
        review = timeStamp = "";
    }

    public Feedback(Long consignmentId, Integer rating, String review, String timeStamp) {
        this.consignmentId = consignmentId;
        this.rating = rating;
        this.review = review;
        this.timeStamp = timeStamp;
    }

    public Feedback(Long id, Long consignmentId, Integer rating, String review, String timeStamp) {
        this.id = id;
        this.consignmentId = consignmentId;
        this.rating = rating;
        this.review = review;
        this.timeStamp = timeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(Long consignmentId) {
        this.consignmentId = consignmentId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", consignmentId=" + consignmentId +
                ", rating=" + rating +
                ", review='" + review + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                '}';
    }
}
