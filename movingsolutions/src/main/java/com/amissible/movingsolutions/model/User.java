package com.amissible.movingsolutions.model;

public class User {

    private Long id;
    private String name;
    private String email;
    private String contact;
    private String password;
    private String timeZone;
    private String timeStamp;

    public User() {
        id = 0L;
        name = email = contact = password = timeZone = timeStamp = "";
    }

    public User(String name, String email, String contact, String password, String timeZone, String timeStamp) {
        this.id = 0L;
        this.name = name;
        this.email = email;
        this.contact = contact;
        this.password = password;
        this.timeZone = timeZone;
        this.timeStamp = timeStamp;
    }

    public User(Long id, String name, String email, String contact, String password, String timeZone, String timeStamp) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.contact = contact;
        this.password = password;
        this.timeZone = timeZone;
        this.timeStamp = timeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", contact='" + contact + '\'' +
                ", password='" + password + '\'' +
                ", timeZone='" + timeZone + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                '}';
    }
}
