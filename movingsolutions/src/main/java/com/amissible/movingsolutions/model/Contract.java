package com.amissible.movingsolutions.model;

public class Contract {

    private Long id;
    private Long consignmentId;
    private String date;
    private String statement;
    private String firstParty;
    private String secondParty;
    private String otherTerms;
    private String url;
    private String signedUrl;

    public Contract() {
        id = consignmentId = 0L;
        date = statement = firstParty = secondParty = otherTerms = url = signedUrl = "";
    }

    public Contract(Long consignmentId, String date, String statement, String firstParty, String secondParty, String otherTerms, String url, String signedUrl) {
        this.consignmentId = consignmentId;
        this.date = date;
        this.statement = statement;
        this.firstParty = firstParty;
        this.secondParty = secondParty;
        this.otherTerms = otherTerms;
        this.url = url;
        this.signedUrl = signedUrl;
    }

    public Contract(Long id, Long consignmentId, String date, String statement, String firstParty, String secondParty, String otherTerms, String url, String signedUrl) {
        this.id = id;
        this.consignmentId = consignmentId;
        this.date = date;
        this.statement = statement;
        this.firstParty = firstParty;
        this.secondParty = secondParty;
        this.otherTerms = otherTerms;
        this.url = url;
        this.signedUrl = signedUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(Long consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getFirstParty() {
        return firstParty;
    }

    public void setFirstParty(String firstParty) {
        this.firstParty = firstParty;
    }

    public String getSecondParty() {
        return secondParty;
    }

    public void setSecondParty(String secondParty) {
        this.secondParty = secondParty;
    }

    public String getOtherTerms() {
        return otherTerms;
    }

    public void setOtherTerms(String otherTerms) {
        this.otherTerms = otherTerms;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSignedUrl() {
        return signedUrl;
    }

    public void setSignedUrl(String signedUrl) {
        this.signedUrl = signedUrl;
    }

    @Override
    public String toString() {
        return "Contract{" +
                "id=" + id +
                ", consignmentId=" + consignmentId +
                ", date='" + date + '\'' +
                ", statement='" + statement + '\'' +
                ", firstParty='" + firstParty + '\'' +
                ", secondParty='" + secondParty + '\'' +
                ", otherTerms='" + otherTerms + '\'' +
                ", url='" + url + '\'' +
                ", signedUrl='" + signedUrl + '\'' +
                '}';
    }
}
