package com.amissible.movingsolutions.model;

public class Payment {

    private Long id;
    private Long userId;
    private Long consignmentId;
    private String txnId;
    private Float txnAmount;
    private String txnStatus;
    private String txnTimeStamp;

    public Payment() {
        id = userId = consignmentId = 0L;
        txnAmount = 0F;
        txnId = txnStatus = txnTimeStamp = "";
    }

    public Payment(Long userId, Long consignmentId, String txnId, Float txnAmount, String txnStatus, String txnTimeStamp) {
        this.userId = userId;
        this.consignmentId = consignmentId;
        this.txnId = txnId;
        this.txnAmount = txnAmount;
        this.txnStatus = txnStatus;
        this.txnTimeStamp = txnTimeStamp;
    }

    public Payment(Long id, Long userId, Long consignmentId, String txnId, Float txnAmount, String txnStatus, String txnTimeStamp) {
        this.id = id;
        this.userId = userId;
        this.consignmentId = consignmentId;
        this.txnId = txnId;
        this.txnAmount = txnAmount;
        this.txnStatus = txnStatus;
        this.txnTimeStamp = txnTimeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(Long consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public Float getTxnAmount() {
        return txnAmount;
    }

    public void setTxnAmount(Float txnAmount) {
        this.txnAmount = txnAmount;
    }

    public String getTxnStatus() {
        return txnStatus;
    }

    public void setTxnStatus(String txnStatus) {
        this.txnStatus = txnStatus;
    }

    public String getTxnTimeStamp() {
        return txnTimeStamp;
    }

    public void setTxnTimeStamp(String txnTimeStamp) {
        this.txnTimeStamp = txnTimeStamp;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", userId=" + userId +
                ", consignmentId=" + consignmentId +
                ", txnId='" + txnId + '\'' +
                ", txnAmount=" + txnAmount +
                ", txnStatus='" + txnStatus + '\'' +
                ", txnTimeStamp='" + txnTimeStamp + '\'' +
                '}';
    }
}
