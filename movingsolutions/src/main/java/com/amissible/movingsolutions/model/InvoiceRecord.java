package com.amissible.movingsolutions.model;

public class InvoiceRecord {

    private Long id;
    private Long invoiceId;
    private String description;
    private Integer quantity;
    private Float unitPrice;
    private Float amount;

    public InvoiceRecord() {
        id = invoiceId = 0L;
        description = "";
        quantity = 0;
        unitPrice = amount = 0F;
    }

    public InvoiceRecord(Long invoiceId, String description, Integer quantity, Float unitPrice, Float amount) {
        this.invoiceId = invoiceId;
        this.description = description;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.amount = amount;
    }

    public InvoiceRecord(Long id, Long invoiceId, String description, Integer quantity, Float unitPrice, Float amount) {
        this.id = id;
        this.invoiceId = invoiceId;
        this.description = description;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public void calculateAmount() {
        amount = quantity * unitPrice;
    }

    @Override
    public String toString() {
        return "InvoiceRecord{" +
                "id=" + id +
                ", invoiceId=" + invoiceId +
                ", description='" + description + '\'' +
                ", quantity=" + quantity +
                ", unitPrice=" + unitPrice +
                ", amount=" + amount +
                '}';
    }
}
