package com.amissible.movingsolutions.model;

public class Complaint {

    private Long id;
    private Long consignmentId;
    private String summary;
    private String details;
    private String resolution;
    private String status;
    private String timeStamp;

    public Complaint() {
        id = consignmentId = 0L;
        summary = details = resolution = status = timeStamp = "";
    }

    public Complaint(Long consignmentId, String summary, String details, String resolution, String status, String timeStamp) {
        this.consignmentId = consignmentId;
        this.summary = summary;
        this.details = details;
        this.resolution = resolution;
        this.status = status;
        this.timeStamp = timeStamp;
    }

    public Complaint(Long id, Long consignmentId, String summary, String details, String resolution, String status, String timeStamp) {
        this.id = id;
        this.consignmentId = consignmentId;
        this.summary = summary;
        this.details = details;
        this.resolution = resolution;
        this.status = status;
        this.timeStamp = timeStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(Long consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "Complaint{" +
                "id=" + id +
                ", consignmentId=" + consignmentId +
                ", summary='" + summary + '\'' +
                ", details='" + details + '\'' +
                ", resolution='" + resolution + '\'' +
                ", status='" + status + '\'' +
                ", timeStamp='" + timeStamp + '\'' +
                '}';
    }
}
