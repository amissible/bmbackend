package com.amissible.movingsolutions.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JwtToken {
    private final String token;

    public JwtToken(String token) {
        this.token = token;
    }

    @JsonProperty("id_token")
    public String getToken() {
        return token;
    }

}
