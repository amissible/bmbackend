package com.amissible.movingsolutions.model;

public class Question {

    private Long id;
    private String question;
    private String status;

    public Question() {
        id = 0L;
        question = status = "";
    }

    public Question(String question, String status) {
        this.question = question;
        this.status = status;
    }

    public Question(Long id, String question, String status) {
        this.id = id;
        this.question = question;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
