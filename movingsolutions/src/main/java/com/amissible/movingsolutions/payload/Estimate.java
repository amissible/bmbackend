package com.amissible.movingsolutions.payload;

public class Estimate {

    private String movingFromZip;
    private String movingToZip;
    private String serviceType;
    private String movingDate;
    private String name;
    private String email;
    private String contact;
    private String timeZone;

    public Estimate() {
    }

    public Estimate(String movingFromZip, String movingToZip, String serviceType, String movingDate, String name, String email, String contact, String timeZone) {
        this.movingFromZip = movingFromZip;
        this.movingToZip = movingToZip;
        this.serviceType = serviceType;
        this.movingDate = movingDate;
        this.name = name;
        this.email = email;
        this.contact = contact;
        this.timeZone = timeZone;
    }

    public String getMovingFromZip() {
        return movingFromZip;
    }

    public void setMovingFromZip(String movingFromZip) {
        this.movingFromZip = movingFromZip;
    }

    public String getMovingToZip() {
        return movingToZip;
    }

    public void setMovingToZip(String movingToZip) {
        this.movingToZip = movingToZip;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getMovingDate() {
        return movingDate;
    }

    public void setMovingDate(String movingDate) {
        this.movingDate = movingDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Override
    public String toString() {
        return "Estimate{" +
                "movingFromZip='" + movingFromZip + '\'' +
                ", movingToZip='" + movingToZip + '\'' +
                ", serviceType='" + serviceType + '\'' +
                ", movingDate='" + movingDate + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", contact='" + contact + '\'' +
                ", timeZone='" + timeZone + '\'' +
                '}';
    }
}
