package com.amissible.movingsolutions.payload;

public class Questionary {

    private Long id;
    private String question;
    private String response;

    public Questionary() {
    }

    public Questionary(String question, String response) {
        this.question = question;
        this.response = response;
    }

    public Questionary(Long id, String question, String response) {
        this.id = id;
        this.question = question;
        this.response = response;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "Questionary{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", response='" + response + '\'' +
                '}';
    }
}
