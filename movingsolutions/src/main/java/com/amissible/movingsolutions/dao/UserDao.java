package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.User;

import java.util.List;

public interface UserDao {
    void createTable();
    List<User> getAll();
    List<User> getAllByTimeStamp(String timeStamp);
    User getById(Long id);
    User getByEmail(String email);
    void update(User user);
    void save(User user);
    void delete(Long id);
    void deleteByEmail(String email);
    void deleteAll();
    void dropTable();
}
