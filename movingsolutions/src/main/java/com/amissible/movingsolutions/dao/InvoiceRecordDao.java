package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.InvoiceRecord;

import java.util.List;

public interface InvoiceRecordDao {
    void createTable();
    List<InvoiceRecord> getAll();
    List<InvoiceRecord> getAllByInvoiceId(Long invoiceId);
    InvoiceRecord getById(Long id);
    void update(InvoiceRecord invoiceRecord);
    void save(InvoiceRecord invoiceRecord);
    void delete(Long id);
    void deleteByInvoiceId(Long invoiceId);
    void deleteAll();
    void dropTable();
}
