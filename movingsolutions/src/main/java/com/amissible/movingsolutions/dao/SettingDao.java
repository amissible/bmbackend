package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.Setting;

import java.util.List;

public interface SettingDao {
    public String get(String key);
    public void refresh();
    public void createTable();
    public List<Setting> getAllSettings();
    public Setting getSettingById(Long id);
    public Setting getSettingByKey(String key);
    public void update(Setting setting);
    public void save(Setting setting);
    public void delete(Long id);
    public void deleteAll();
    public void dropTable();
}