package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.Invoice;

import java.util.List;

public interface InvoiceDao {
    void createTable();
    List<Invoice> getAll();
    List<Invoice> getAllByDate(String date);
    List<Invoice> getAllByDueDate(String dueDate);
    List<Invoice> getAllByStatus(String status);
    Invoice getById(Long id);
    Invoice getByConsignmentId(Long consignmentId);
    List<Invoice> getAllByConsignmentId(Long consignmentId);
    void update(Invoice invoice);
    void save(Invoice invoice);
    void delete(Long id);
    void deleteByConsignmentId(Long consignmentId);
    void deleteByStatus(String status);
    void deleteAll();
    void dropTable();
}
