package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.Complaint;

import java.util.List;

public interface ComplaintDao {
    void createTable();
    List<Complaint> getAll();
    List<Complaint> getAllByStatus(String status);
    List<Complaint> getAllByTimeStamp(String timeStamp);
    Complaint getById(Long id);
    Complaint getByConsignmentId(Long consignmentId);
    void update(Complaint complaint);
    void save(Complaint complaint);
    void delete(Long id);
    void deleteByConsignmentId(Long consignmentId);
    void deleteAll();
    void dropTable();
}
