package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.Task;

import java.util.List;

public interface TaskDao {
    void createTable();
    List<Task> getAll();
    List<Task> getByDatedTo(Long employeeId,String datedTo);
    Task getById(Long userId);
    List<Task> getByUserId(Long userId);
    List<Task> getByConsignmentId(Long consignmentId);
    List<Task> getByEmployeeId(Long employeeId);
    List<Task> getPendingByEmployeeId(Long employeeId);
    List<Task> getByStatus(String status);
    void update(Task task);
    void save(Task task);
    void delete(Long id);
    void deleteByUserId(Long user_id);
    void deleteByConsignmentId(Long consignment_id);
    void deleteByEmployeeId(Long employee_id);
    void deleteByStatus(String status);
    void deleteAll();
    void dropTable();
}
