package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.Payment;

import java.util.List;

public interface PaymentDao {
    void createTable();
    List<Payment> getAll();
    List<Payment> getAllByUserId(Long userId);
    List<Payment> getAllByConsignmentId(Long consignmentId);
    List<Payment> getAllByTxnStatus(String txnStatus);
    List<Payment> getAllByTxnTimeStamp(String txnTimeStamp);
    Payment getById(Long id);
    void update(Payment payment);
    void save(Payment payment);
    void delete(Long id);
    void deleteByUserId(Long userId);
    void deleteByConsignmentId(Long consignmentId);
    void deleteAll();
    void dropTable();
}
