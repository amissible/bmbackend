package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.Employee;

import java.util.List;

public interface EmployeeDao {
    void createTable();
    List<Employee> getAll();
    List<Employee> getAllByRole(String role);
    List<Employee> getAllByTimeStamp(String timeStamp);
    Employee getById(Long id);
    Employee getByEmail(String email);
    void update(Employee employee);
    void save(Employee employee);
    void delete(Long id);
    void deleteByEmail(String email);
    void deleteAll();
    void dropTable();
}
