package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.Question;

import java.util.List;

public interface QuestionDao {
    void createTable();
    List<Question> getAll();
    List<Question> getAllByStatus(String status);
    Question getById(Long id);
    void update(Question question);
    void save(Question question);
    void delete(Long id);
    void deleteByStatus(String status);
    void deleteAll();
    void dropTable();
}
