package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.Questionnaire;

import java.util.List;

public interface QuestionnaireDao {
    void createTable();
    List<Questionnaire> getAll();
    List<Questionnaire> getAllByConsignmentId(Long consignmentId);
    List<Questionnaire> getAllByQuestionId(Long questionId);
    Questionnaire getById(Long id);
    Questionnaire get(Long consignmentId,Long questionId);
    void update(Questionnaire questionnaire);
    void save(Questionnaire questionnaire);
    void delete(Long id);
    void deleteByConsignmentId(Long consignmentId);
    void deleteByQuestionId(Long questionId);
    void del(Long consignmentId,Long questionId);
    void deleteAll();
    void dropTable();
}
