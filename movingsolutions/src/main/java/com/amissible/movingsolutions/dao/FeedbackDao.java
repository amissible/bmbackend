package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.Feedback;

import java.util.List;

public interface FeedbackDao {
    void createTable();
    List<Feedback> getAll();
    List<Feedback> getAllByTimeStamp(String timeStamp);
    Feedback getById(Long id);
    Feedback getByConsignmentId(Long consignmentId);
    void update(Feedback feedback);
    void save(Feedback feedback);
    void delete(Long id);
    void deleteByConsignmentId(Long consignmentId);
    void deleteAll();
    void dropTable();
}
