package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.Contract;

import java.util.List;

public interface ContractDao {
    void createTable();
    List<Contract> getAll();
    List<Contract> getAllByDate(String date);
    Contract getById(Long id);
    Contract getByConsignmentId(Long consignmentId);
    void update(Contract contract);
    void save(Contract contract);
    void delete(Long id);
    void deleteByConsignmentId(Long consignmentId);
    void deleteAll();
    void dropTable();
}
