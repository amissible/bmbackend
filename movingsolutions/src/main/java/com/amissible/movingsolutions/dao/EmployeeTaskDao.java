package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.EmployeeTask;

import java.util.List;

public interface EmployeeTaskDao {
    void createTable();
    List<EmployeeTask> getAll();
    List<EmployeeTask> getAllByEmployeeId(Long employeeId);
    List<EmployeeTask> getAllByConsignmentId(Long consignmentId);
    List<EmployeeTask> getAllByStatus(String status);
    List<EmployeeTask> getAllByEC(Long employeeId,Long consignmentId);
    List<EmployeeTask> getAllByES(Long employeeId,String status);
    List<EmployeeTask> getAllByCS(Long consignmentId,String status);
    List<EmployeeTask> getAllByECS(Long employeeId,Long consignmentId,String status);
    EmployeeTask getById(Long id);
    void update(EmployeeTask employeeTask);
    void save(EmployeeTask employeeTask);
    void delete(Long id);
    void deleteByEmployeeId(Long employeeId);
    void deleteByConsignmentId(Long consignmentId);
    void deleteByStatus(String status);
    void deleteAll();
    void dropTable();
}
