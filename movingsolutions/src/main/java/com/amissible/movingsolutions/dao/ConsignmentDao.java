package com.amissible.movingsolutions.dao;

import com.amissible.movingsolutions.model.Consignment;

import java.util.List;

public interface ConsignmentDao {
    void createTable();
    List<Consignment> getAll();
    List<Consignment> getAllByUserId(Long userId);
    List<Consignment> getAllByMovingDate(String movingDate);
    List<Consignment> getAllByServiceType(String serviceType);
    List<Consignment> getAllByStatus(String status);
    List<Consignment> getAllByTimeStamp(String timeStamp);
    Consignment getById(Long id);
    Long getUserId(Long consignmentId);
    void update(Consignment consignment);
    void save(Consignment consignment);
    void delete(Long id);
    void deleteByUserId(Long userId);
    void deleteAll();
    void dropTable();
    String setStatus(Long id);
}
