package com.amissible.movingsolutions.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.Timestamp;

@RestController
@ControllerAdvice
public class AppEntityExcpetionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(AppException.class)
    public final ResponseEntity<Object> handleAllExceptions(AppException ex, WebRequest request){
        ExceptionResponse response =new ExceptionResponse(new Timestamp(System.currentTimeMillis()),ex.getMessage(),request.getDescription(false));
        return new ResponseEntity<>(response, ex.getHttpStatus());
    }

}