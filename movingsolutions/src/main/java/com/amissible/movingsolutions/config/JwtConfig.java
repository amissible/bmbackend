package com.amissible.movingsolutions.config;

import com.amissible.movingsolutions.config.jwt.JWTFilter;
import com.amissible.movingsolutions.config.jwt.TokenProvider;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JwtConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
    private TokenProvider tokenProvider;
    public JwtConfig(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    @Override
    public void configure(HttpSecurity web) {
        JWTFilter customFilter = new JWTFilter(tokenProvider);
        web.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
