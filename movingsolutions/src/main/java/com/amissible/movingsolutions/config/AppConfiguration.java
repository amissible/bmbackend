package com.amissible.movingsolutions.config;

import com.amissible.movingsolutions.model.Mail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Configuration
public class AppConfiguration {

    @Value("${timezone}")
    String timeZone;

    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
    }

    @Bean
    BlockingQueue<Mail> mailBlockingQueue() {
        return new LinkedBlockingQueue<>();
    }
}
