package com.amissible.movingsolutions.config.email;

import com.amissible.movingsolutions.controller.UserController;
import com.amissible.movingsolutions.model.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.concurrent.BlockingQueue;

@Component
public class Publisher {
    @Autowired
    BlockingQueue<Mail> mailBlockingQueue;

    private static final Logger logger = LoggerFactory.getLogger(Publisher.class);

    public void setMailBlockingQueue(Mail mail) throws InterruptedException {
        mailBlockingQueue.put(mail);
        logger.info("The mail queue has " + mailBlockingQueue.size() + " mails in queue.");
    }
}
