package com.amissible.movingsolutions.config.email;

import com.amissible.movingsolutions.model.Mail;
import com.amissible.movingsolutions.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
public class Consumer {

    @Autowired
    BlockingQueue<Mail> mailBlockingQueue;

    @Autowired
    EmailService emailService;

    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);

    AtomicBoolean active = new AtomicBoolean(true);

    WorkerThread workerThread;

    @PostConstruct
    public void initConsumer() {
        workerThread = new WorkerThread();
        workerThread.start();
    }

    class WorkerThread extends Thread {
        public void run() {
            while(active.get()) {
                try {
                    Mail mail = mailBlockingQueue.take();
                    emailService.sendSimpleMessage(mail);
                    logger.info("The mail queue has " + mailBlockingQueue.size() + " mails remaining in queue.");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
