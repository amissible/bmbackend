package com.amissible.movingsolutions.controller;

import com.amissible.movingsolutions.config.jwt.JWTFilter;
import com.amissible.movingsolutions.config.jwt.TokenProvider;
import com.amissible.movingsolutions.dao.EmployeeDao;
import com.amissible.movingsolutions.model.Employee;
import com.amissible.movingsolutions.model.JwtToken;
import com.amissible.movingsolutions.service.UserService;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import com.amissible.movingsolutions.model.UserPayload;
import com.amissible.movingsolutions.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    TokenProvider tokenProvider;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUserDetailsService userDetailsService;

    @Autowired
    EmployeeDao employeeDao;

    @Autowired
    UserService userService;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    public LoginController(AuthenticationManagerBuilder authenticationManagerBuilder) {
        this.authenticationManagerBuilder = authenticationManagerBuilder;
    }

    @PostMapping("/user")
    public ResponseEntity<?> createAuthenticationUserToken(@RequestBody UserPayload user) throws Exception {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.createToken(authentication, false);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);

        return new ResponseEntity<>(new JwtToken(jwt), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/forget/password")
    public ResponseEntity reset(@RequestParam String email) throws InterruptedException {
        userService.resetPassword(email);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/create/dummy/employee")
    public ResponseEntity createDummy() {
        Employee employee = new Employee("Michael", "michael@gmail.com", "4865550100", encoder.encode("michaeltownley"), "EMPLOYEE", "25/2/2021");
        employeeDao.save(employee);
        return new ResponseEntity(HttpStatus.OK);
    }
}