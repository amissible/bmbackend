package com.amissible.movingsolutions.controller;

import com.amissible.movingsolutions.dao.QuestionDao;
import com.amissible.movingsolutions.model.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/question")
public class QuestionsController {

    @Autowired
    private QuestionDao questionDao;

    //add question
    @PostMapping("/add")
    public ResponseEntity addQuestion(@RequestBody Question question) {
        questionDao.save(question);
        return new ResponseEntity(HttpStatus.OK);
    }

    //get by id
    @GetMapping("/get_by_id")
    public ResponseEntity getQuestions(Long id) {
        return new ResponseEntity(questionDao.getById(id), HttpStatus.OK);
    }

    //get all questions
    @GetMapping("/get_all")
    public ResponseEntity getAllQuestions() {
        return new ResponseEntity(questionDao.getAll(), HttpStatus.OK);
    }
}
