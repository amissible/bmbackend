package com.amissible.movingsolutions.controller;

import com.amissible.movingsolutions.dao.ConsignmentDao;
import com.amissible.movingsolutions.dao.UserDao;
import com.amissible.movingsolutions.model.User;
import com.amissible.movingsolutions.payload.Estimate;
import com.amissible.movingsolutions.service.EstimateService;
import com.amissible.movingsolutions.service.recaptcha.CaptchaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/estimate")
public class EstimateController {

    @Autowired
    CaptchaService captchaService;

    @Autowired
    private EstimateService estimateService;

    private static final Logger logger = LoggerFactory.getLogger(EstimateService.class);

    @PostMapping("/calculate_estimate")
    public ResponseEntity calculateEstimate(@RequestBody Estimate estimate, HttpServletRequest request) throws Exception {
       // String response = request.getParameter("g-recaptcha-response");
       // captchaService.processResponse(response);
        return new ResponseEntity(estimateService.sendEstimateByEmail(estimate), HttpStatus.OK);
    }
}
