package com.amissible.movingsolutions.controller;

import com.amissible.movingsolutions.config.email.Publisher;
import com.amissible.movingsolutions.dao.ConsignmentDao;
import com.amissible.movingsolutions.dao.UserDao;
import com.amissible.movingsolutions.model.*;
import com.amissible.movingsolutions.service.ComplaintService;
import com.amissible.movingsolutions.service.FeedbackService;
import com.amissible.movingsolutions.service.PaymentService;
import com.amissible.movingsolutions.service.TestSushmitaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/test/sushmita")
public class TestSushmitaController {

    @Autowired
    private ConsignmentDao consignmentDao;

    @Autowired
    TestSushmitaServices testSushmitaServices;

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private ComplaintService complaintService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    Publisher publisher;

    @Autowired
    UserDao userDao;

    @RequestMapping("/saveFeedback")
    @ResponseBody
    public void saveFeedback(@RequestBody Feedback feedback){
        feedbackService.saveFeedback(feedback);
    }

    @RequestMapping("/getFeedback")
    @ResponseBody
    public Feedback getFeedback(@RequestParam Long consignmentId){
        return feedbackService.getFeedback(consignmentId);
    }

    @RequestMapping("/saveComplaint")
    @ResponseBody
    public void saveComplaint(@RequestBody Complaint complaint){
        feedbackService.saveComplaint(complaint);
    }

    @RequestMapping("/getComplaint")
    @ResponseBody
    public Complaint getComplaint(@RequestParam Long consignmentId){
        return feedbackService.getComplaint(consignmentId);
    }

    @GetMapping("/approve")
    public ResponseEntity<?> Approve(@RequestParam Long consignmentId) throws InterruptedException {
        Consignment consignment = consignmentDao.getById(consignmentId);
        if(consignment == null)
            return new ResponseEntity("The consignment does not exist.", HttpStatus.BAD_REQUEST);
        else if(consignment.getStatus().equalsIgnoreCase("approved"))
            return new ResponseEntity("The consignment has already been approved", HttpStatus.BAD_REQUEST);
        else if(consignment.getStatus().equalsIgnoreCase("waiting approval")){
            consignment.setStatus("APPROVED");
            Long userId = consignment.getUserId();
            User user = userDao.getById(userId);
            Mail mail = new Mail();
            mail.setTo(user.getEmail());
            mail.setCc("amit.shukla@amissible.com,anwar.mehmood@amissible.com");
            mail.setBcc("omendra.sgakkar@amissible.com,urvashi.dubey@amissible.com,sushmita.singh@amissible.com," +
                    "sparsh.mishra@amissible.com");
            mail.setSubject("Consignment Approved");
            mail.setBody("Your consignment has been approved, kindly ensure all dues are cleared.");
            publisher.setMailBlockingQueue(mail);
            consignmentDao.update(consignment);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Consignment is not yet ready.", HttpStatus.FORBIDDEN);
        }
    }
}
