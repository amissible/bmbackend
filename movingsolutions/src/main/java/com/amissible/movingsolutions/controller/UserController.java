package com.amissible.movingsolutions.controller;

import com.amissible.movingsolutions.config.jwt.JWTFilter;
import com.amissible.movingsolutions.dao.*;
import com.amissible.movingsolutions.model.*;
import com.amissible.movingsolutions.service.PaymentService;
import com.amissible.movingsolutions.service.PdfDownloadService;
import com.amissible.movingsolutions.service.ReportService;
import com.amissible.movingsolutions.service.UserService;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private InvoiceDao invoiceDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ConsignmentDao consignmentDao;

    @Autowired
    private UserService userService;

    @Autowired
    JWTFilter jwtFilter;

    @Autowired
    PaymentService paymentService;

    @Autowired
    ContractDao contractDao;

    @Autowired
    PdfDownloadService downloadService;

    @Autowired
    ReportService reportService;

    String path = "/app/export/movsol";

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    //get all consignments
    @GetMapping("consignments/get_all")
    public ResponseEntity<?> getConsignmentList(HttpServletRequest httpServletRequest) {
        User user = userService.getUserFromToken(jwtFilter.resolveToken(httpServletRequest));
        //if user does not exist
        if(user == null)
            return new ResponseEntity<>("User does not exist", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(consignmentDao.getAllByUserId(user.getId()), HttpStatus.OK);
    }

    //get single consignment
    @GetMapping("consignments/get")
    public ResponseEntity<?> getConsignment(@RequestParam Long consignmentId) {
        return new ResponseEntity<>(consignmentDao.getById(consignmentId), HttpStatus.OK);
    }

    @GetMapping("/consignment/update")
    public ResponseEntity<?> updateConsignment(@RequestBody Consignment consignment) {
        consignmentDao.update(consignment);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/change/password")
    public ResponseEntity<?> changePassword(@RequestBody List<UserPayload> userPayload) {
        return userService.changePassword(userPayload);
    }

    @GetMapping("/invoice/view")
    public ResponseEntity<?> getInvoice(@RequestParam Long consignmentId) {
        Invoice invoice = invoiceDao.getByConsignmentId(consignmentId);
        if(invoice == null)
            return new ResponseEntity<>("Invoice has not been generated for the requested consignment.", HttpStatus.BAD_REQUEST);
        List<InvoiceRecord> invoiceRecordList = invoiceRecordDao.getAllByInvoiceId(invoice.getId());
        InvoicePayload invoicePayload = new InvoicePayload(invoice.getId(), invoice.getConsignmentId(), invoice.getBillAddress(),
                invoice.getShipAddress(), invoice.getDate(), invoice.getDueDate(), invoice.getStatus(), invoice.getSubTotal(),
                invoice.getDiscount(), invoice.getTax(), invoice.getTotal(), invoiceRecordList);
        return new ResponseEntity<>(invoicePayload, HttpStatus.OK);
    }

    @GetMapping("/invoice/view_all")
    public ResponseEntity<?> getAllInvoices() {
        return new ResponseEntity<>(invoiceDao.getAll(), HttpStatus.OK);
    }

    @GetMapping("/download/invoice")
    public ResponseEntity<?> downloadInvoice(@RequestParam Long consignmentId) {
        Invoice invoice = invoiceDao.getByConsignmentId(consignmentId);
        try {
            logger.info("Attempting to download invoice.");
            return (ResponseEntity<?>) downloadService.downloadPdf(path + "/invoice/invoice_" + invoice.getId() + ".pdf");
        } catch (FileNotFoundException exception) {
            return new ResponseEntity<>("Invoice has not been generated for the requested consignment.", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/get/info")
    public ResponseEntity<?> getInfo(HttpServletRequest httpServletRequest) {
        return new ResponseEntity<>(userService.getUserFromToken(jwtFilter.resolveToken(httpServletRequest)), HttpStatus.OK);
    }

    @PostMapping("/update/info")
    public ResponseEntity<?> updateInfo(@RequestBody User user) {
        userDao.update(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    //returns list of payments done per consignment
    @GetMapping("/view/payments")
    public ResponseEntity<?> viewPayments(@RequestParam Long consignmentId) {
        return new ResponseEntity<>(paymentService.getPaymentListByConsignment(consignmentId), HttpStatus.OK);
    }

    //returns complete list of payments done by user
    @GetMapping("/view/all_payments")
    public ResponseEntity<?> viewAllPayments(HttpServletRequest httpServletRequest) {
        Long userId = userService.getUserId(jwtFilter.resolveToken(httpServletRequest));
        return new ResponseEntity<>(paymentService.getPaymentListByUserId(userId), HttpStatus.OK);
    }

    //returns pdf of a specific payment
    @GetMapping("/download/payment_receipt")
    public ResponseEntity<?> downloadReceipt(@RequestParam Long paymentId) throws FileNotFoundException, JRException {
        try {
            logger.info("Requested payment receipt.");
            return downloadService.downloadPdf(path + "/payment/payment_" + paymentId + ".pdf");
        } catch (FileNotFoundException exception) {
            return new ResponseEntity<>("Payment receipt has not been generated for the requested consignment.", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/download/signature")
    public ResponseEntity<?> getSignature(HttpServletRequest httpServletRequest) {
        User user = userService.getUserFromToken(jwtFilter.resolveToken(httpServletRequest));
        //if user does not exist
        if(user == null)
            return new ResponseEntity<>("User does not exist", HttpStatus.BAD_REQUEST);
        try {
            return downloadService.downloadImage(path + "/user_signatures/" + user.getEmail() + "_signature.jpg");
        } catch (Exception e) {
            return new ResponseEntity<>("No existing signature found!", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/upload/signature")
    public String uploadFile(@RequestParam Long consignmentId, @RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
        String imgPath = null;
        try {
            Path copyLocation = Paths
                    .get(path + File.separator + "user_signatures" + File.separator +  StringUtils.cleanPath(userService.getUserFromToken(
                            jwtFilter.resolveToken(request)).getEmail() + "_signature." +
                            FilenameUtils.getExtension(file.getOriginalFilename())));
            Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
            imgPath = copyLocation.toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Could not store file " + file.getOriginalFilename()
                    + ". Please try again!");
        }
        Contract contract = contractDao.getByConsignmentId(consignmentId);
        contract.setSignedUrl(imgPath);
        contractDao.update(contract);
        return reportService.signContract(contract, path);
    }

    //returns a contract for a specific consignment id
    @GetMapping("/download/contract")
    public ResponseEntity<?> downloadContract(@RequestParam Long consignmentId) {
        Contract contract = contractDao.getByConsignmentId(consignmentId);
        if(contract == null)
            return new ResponseEntity<>("No contract exists for consignment id: " + consignmentId, HttpStatus.BAD_REQUEST);
        try {
            logger.info("Attempting to download signed contract.");
            return downloadService.downloadPdf(path + "/contract/signed_contract_" + contract.getId() + ".pdf");
        } catch (FileNotFoundException exception) {
            try {
                logger.info("Signed signature not found. Attempting to download un_signed contract.");
                return downloadService.downloadPdf(path + "/contract/contract_" + contract.getId() + ".pdf");
            } catch (FileNotFoundException e) {
                logger.warn("No contract exists for consignment id: " + consignmentId);
                return new ResponseEntity<>("Contract has not been generated for the requested consignment.", HttpStatus.BAD_REQUEST);
            }
        }
    }

    @GetMapping("/get/contract")
    public ResponseEntity<?> getContract(@RequestParam Long consignmentId) {
        Contract contract = contractDao.getByConsignmentId(consignmentId);
        if(contract == null)
            return new ResponseEntity<>("Contract has not been generated for the requested consignment.", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(contract, HttpStatus.OK);
    }

    //test or sample codes ===================================================================================================

    @GetMapping("/sample/contract")
    public ResponseEntity<?> genContract() throws FileNotFoundException, JRException {
        Contract contract = contractDao.getByConsignmentId(88L);
        reportService.exportContract("pdf", contract, path);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Autowired
    PaymentDao paymentDao;

    @GetMapping("/sample/receipt")
    public ResponseEntity<?> genReceipt() throws FileNotFoundException, JRException {
        Payment payment = paymentDao.getById(7L);
        reportService.exportPayment("pdf", payment, path);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //sample code to test invoice
    @Autowired
    InvoiceRecordDao invoiceRecordDao;

    @PostMapping("/sample/invoice")
    public ResponseEntity generateInvoiceReport(Long consignmentId) throws FileNotFoundException, JRException {
        Invoice invoice = new Invoice();
        userDao.getById(consignmentDao.getUserId(consignmentId));
        invoice.setConsignmentId(consignmentId);
        invoice.setBillAddress("ABC");
        invoice.setShipAddress("DEF");
        invoice.setDueDate("Today");
        invoice.setStatus("DUE");
        invoice.setSubTotal(5599.0f);
        invoice.setTax(0.0f);
        invoice.setTotal(5599.0f);
        invoiceDao.save(invoice);

        //re-get invoice
        invoice = invoiceDao.getByConsignmentId(consignmentId);
        InvoiceRecord invoiceRecord = new InvoiceRecord(invoice.getId(), "Bed", 2, 50.0f, 0.0f);
        invoiceRecord.calculateAmount();

        //sample invoice-records
        invoiceRecord.setDescription("Table");
        invoiceRecordDao.save(invoiceRecord);
        invoiceRecord.setDescription("Double Bed");
        invoiceRecordDao.save(invoiceRecord);
        invoiceRecord.setDescription("Car");
        invoiceRecordDao.save(invoiceRecord);

        InvoicePayload invoicePayload = new InvoicePayload(invoice.getId(), invoice.getConsignmentId(), invoice.getBillAddress(),
                invoice.getShipAddress(), invoice.getDate(), invoice.getDueDate(), invoice.getStatus(), invoice.getSubTotal(),
                invoice.getDiscount(), invoice.getTax(), invoice.getTotal(), invoiceRecordDao.getAllByInvoiceId(invoice.getId()));

        return new ResponseEntity(reportService.exportInvoice("pdf", invoicePayload, path), HttpStatus.OK);
    }
}