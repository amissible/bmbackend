package com.amissible.movingsolutions.controller;

import com.amissible.movingsolutions.dao.ConsignmentDao;
import com.amissible.movingsolutions.dao.QuestionnaireDao;
import com.amissible.movingsolutions.model.Questionnaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/questionnaire")
public class QuestionnaireController {
    @Autowired
    private QuestionnaireDao questionnaireDao;

    @Autowired
    private ConsignmentDao consignmentDao;

    //one by one approach
    @PostMapping("/submit")
    public ResponseEntity submit(@RequestBody Questionnaire questionnaire) {
        questionnaireDao.save(questionnaire);
        return new ResponseEntity(HttpStatus.OK);
    }

    //List approach
    @PostMapping("/submit_all")
    ResponseEntity submit(@RequestBody List<Questionnaire> questionnaireList) {
        for(Questionnaire itr : questionnaireList)
            questionnaireDao.save(itr);
        return new ResponseEntity(HttpStatus.OK);
    }

    //for testing
    @GetMapping("/get_all")
    ResponseEntity get() {
        return new ResponseEntity(questionnaireDao.getAll(), HttpStatus.OK);
    }
}
