package com.amissible.movingsolutions.controller;

import com.amissible.movingsolutions.dao.SettingDao;
import com.amissible.movingsolutions.dao.TaskDao;
import com.amissible.movingsolutions.model.*;
import com.amissible.movingsolutions.payload.Questionary;
import com.amissible.movingsolutions.service.TestOmendraServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@CrossOrigin
@RestController
@RequestMapping("/omendra")
public class TestOmendraController {

    @Autowired
    TestOmendraServices testOmendraServices;

    // BM-16
    @RequestMapping("/getUserDetails")
    public User getUserDetails(@RequestParam Long id){
        return testOmendraServices.getUserDetails(id);
    }

    @RequestMapping("/getUserDetailsByEmail")
    public User getUserDetailsByEmail(@RequestParam String email){
        return testOmendraServices.getUserDetailsByEmail(email);
    }

    @RequestMapping("/updateUserDetails")
    public void updateUserDetails(@RequestParam User user){
        testOmendraServices.updateUserDetails(user);
    }

    @RequestMapping("/updateUserPassword")
    public void updateUserPassword(@RequestParam Long id,@RequestParam String oldPassword,@RequestParam String newPassword){
        testOmendraServices.updateUserPassword(id,oldPassword,newPassword);
    }

    // BM-20
    @RequestMapping("/makePayment")
    public RedirectView makePayment(@RequestParam Long consignmentId, @RequestParam Float amount){
        return testOmendraServices.makePayment(consignmentId,amount);
    }
    @RequestMapping("/success")
    public RedirectView success(@RequestParam String token,@RequestParam String state,@RequestParam Float amount,@RequestParam String paymentId){
        return testOmendraServices.success(Long.parseLong(token),state,amount,paymentId);
    }
    @RequestMapping("/cancel")
    public RedirectView cancel(@RequestParam String token){
        return testOmendraServices.cancel(Long.parseLong(token));
    }

    // BM-30
    @RequestMapping("/getPaymentData")
    public List<Payment> getPaymentData(){
        return testOmendraServices.getPaymentData();
    }

    // BM-25
    @RequestMapping("/getComplaintData")
    public List<Complaint> getComplaintData(){
        return testOmendraServices.getComplaintData();
    }
    @RequestMapping("/updateComplaint")
    public void updateComplaint(@RequestBody Complaint complaint){
        testOmendraServices.updateComplaint(complaint);
    }

    // BM-28
    @RequestMapping("/getFeedbackData")
    public List<Feedback> getFeedbackData(){
        return testOmendraServices.getFeedbackData();
    }

    // BM-33
    @RequestMapping("/getUser")
    public User getUser(@RequestParam Long userId){
        return testOmendraServices.getUser(userId);
    }
    @RequestMapping("/getUserData")
    public List<User> getUserData(){
        return testOmendraServices.getUserData();
    }
    @RequestMapping("/updateUser")
    public void updateUser(@RequestBody User user){
        testOmendraServices.updateUser(user);
    }

    // BM-
    @RequestMapping("/getEmployeeData")
    public List<Employee> getEmployeeData(){
        return testOmendraServices.getEmployeeData();
    }
    @RequestMapping("/updateEmployee")
    public void updateEmployee(@RequestBody Employee employee){
        testOmendraServices.updateEmployee(employee);
    }
    @RequestMapping("/saveEmployee")
    public Employee saveEmployee(@RequestBody Employee employee){
        return testOmendraServices.saveEmployee(employee);
    }
    @RequestMapping("/getEmployeeByEmail")
    public List<Employee> getEmployeeByEmail(@RequestParam String email){
        return testOmendraServices.getEmployeeByEmail(email);
    }


    // BM-
    @RequestMapping("/getInvoiceData")
    public List<Invoice> getInvoiceData(){
        return testOmendraServices.getInvoiceData();
    }
    @RequestMapping("/updateInvoice")
    public void updateInvoice(@RequestBody Invoice invoice){
        testOmendraServices.updateInvoice(invoice);
    }
    @RequestMapping("/saveInvoice")
    public void saveInvoice(@RequestBody Invoice invoice){
        testOmendraServices.saveInvoice(invoice);
    }
    @RequestMapping("/getInvoiceRecordData")
    public List<InvoiceRecord> getInvoiceRecordData(@RequestParam Long invoiceId){
        return testOmendraServices.getInvoiceRecordData(invoiceId);
    }
    @RequestMapping("/updateInvoiceRecord")
    public void updateInvoiceRecord(@RequestBody InvoiceRecord invoiceRecord){
        testOmendraServices.updateInvoiceRecord(invoiceRecord);
    }
    @RequestMapping("/deleteInvoiceRecord")
    public void deleteInvoiceRecord(@RequestBody InvoiceRecord invoiceRecord){
        testOmendraServices.deleteInvoiceRecord(invoiceRecord);
    }
    @RequestMapping("/saveInvoiceRecord")
    public InvoiceRecord saveInvoiceRecord(@RequestBody InvoiceRecord invoiceRecord){
        return testOmendraServices.saveInvoiceRecord(invoiceRecord);
    }
    @RequestMapping("/generateInvoicePDF")
    public ResponseEntity generateInvoicePDF(@RequestParam Long invoiceId){
        return testOmendraServices.generateInvoicePDF(invoiceId);
    }
    @RequestMapping("/generatePaymentPDF")
    public ResponseEntity generatePaymentPDF(@RequestParam Long paymentId){
        return testOmendraServices.generatePaymentPDF(paymentId);
    }
    @RequestMapping("/generateContractPDF")
    public ResponseEntity generateContractPDF(@RequestParam Long contractId){
        return testOmendraServices.generateContractPDF(contractId);
    }
    @RequestMapping("/getSignedContractPDF")
    public ResponseEntity getSignedContractPDF(@RequestParam Long contractId){
        return testOmendraServices.getSignedContractPDF(contractId);
    }

    //BM-
    @RequestMapping("/getConsignment")
    public Consignment getConsignment(@RequestParam Long consignmentId){
        return testOmendraServices.getConsignment(consignmentId);
    }
    @RequestMapping("/getConsignmentData")
    public List<Consignment> getConsignmentData(){
        return testOmendraServices.getConsignmentData();
    }
    @RequestMapping("/updateConsignment")
    public void updateConsignment(@RequestBody Consignment consignment){
        testOmendraServices.updateConsignment(consignment);
    }
    @RequestMapping("/getContract")
    public Contract getContract(@RequestParam Long consignmentId){
        return testOmendraServices.getContract(consignmentId);
    }
    @RequestMapping("/updateContract")
    public void updateContract(@RequestBody Contract contract){
        testOmendraServices.updateContract(contract);
    }
    @RequestMapping("/getQuestionary")
    public List<Questionary> getQuestionary(@RequestParam Long consignmentId){
        return testOmendraServices.getQuestionary(consignmentId);
    }
    @RequestMapping("/newInvoice")
    public void newInvoice(@RequestParam Long consignmentId){
        testOmendraServices.newInvoice(consignmentId);
    }
    @RequestMapping("/makeRefund")
    public void makeRefund(@RequestParam Long consignmentId,@RequestParam String refundDesc,@RequestParam Float refundAmount){
        testOmendraServices.makeRefund(consignmentId, refundDesc, refundAmount);
    }
    @RequestMapping("/deleteConsignment")
    public void deleteConsignment(@RequestParam Long consignmentId){
        testOmendraServices.deleteConsignment(consignmentId);
    }






    @RequestMapping("/getEmployees")
    public List<Employee> getEmployees(){
        return testOmendraServices.getEmployees();
    }



    //BM-
    @RequestMapping("/getTaskData")
    public List<Task> getTaskData(){
        return testOmendraServices.getTaskData();
    }
    @RequestMapping("/updateTask")
    public void updateTask(@RequestBody Task task){
        testOmendraServices.updateTask(task);
    }
    @RequestMapping("/saveTask")
    public void saveTask(@RequestBody Task task){
        testOmendraServices.saveTask(task);
    }
    @RequestMapping("/deleteTask")
    public void deleteTask(@RequestParam Long taskId){
        testOmendraServices.deleteTask(taskId);
    }
    @RequestMapping("/getTodayTask")
    public List<Task> getTodayTask(@RequestParam String jwt){
        return testOmendraServices.getTodayTask(jwt);
    }
    @RequestMapping("/getPendingTask")
    public List<Task> getPendingTask(@RequestParam String jwt){
        return testOmendraServices.getPendingTask(jwt);
    }
    @RequestMapping("/pushData")
    public void pushData(@RequestParam Long taskId,@RequestParam String data){
        testOmendraServices.pushData(taskId,data);
    }














    @RequestMapping("/test")
    public void test(){
        testOmendraServices.test();
    }


    @RequestMapping("/getTimeZonesOptions")
    public List<String> getTimeZonesOptions(){
        List<String> list = new ArrayList<>();
        Arrays.stream(TimeZone.getAvailableIDs()).iterator().forEachRemaining(data -> list.add(data));
        return list;
    }

    @RequestMapping("/getEmployee")
    public Employee getEmployee(@RequestParam String jwt){
        return testOmendraServices.getEmployee(jwt);
    }

    @RequestMapping("/getCurrency")
    public String getCurrency(){
        return testOmendraServices.getCurrency();
    }

}
