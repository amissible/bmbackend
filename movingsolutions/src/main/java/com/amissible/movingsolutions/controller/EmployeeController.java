package com.amissible.movingsolutions.controller;

import com.amissible.movingsolutions.config.jwt.JWTFilter;
import com.amissible.movingsolutions.config.jwt.TokenProvider;
import com.amissible.movingsolutions.dao.*;
import com.amissible.movingsolutions.model.*;
import io.jsonwebtoken.Claims;
import net.sf.jasperreports.engine.JRException;
import org.jfree.base.modules.PackageSorter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;

@CrossOrigin
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeDao employeeDao;

    @Autowired
    JWTFilter jwtFilter;

    @Autowired
    TokenProvider tokenProvider;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    ConsignmentDao consignmentDao;

    @Autowired
    InvoiceDao invoiceDao;

    @Autowired
    UserDao userDao;

    @Autowired
    InvoiceRecordDao invoiceRecordDao;

    @GetMapping("/get/info")
    public ResponseEntity getEmployeeInfo(HttpServletRequest httpServletRequest) {
        String jwt = jwtFilter.resolveToken(httpServletRequest);
        Claims claims = tokenProvider.getClaims(jwt);
        return new ResponseEntity(employeeDao.getByEmail(claims.getSubject()), HttpStatus.OK);
    }

    @GetMapping("/generate/invoice")
    public ResponseEntity createInvoice(Long consignmentId) {
        Consignment consignment = consignmentDao.getById(consignmentId);
        Invoice invoice = new Invoice();
        invoice.setConsignmentId(consignment.getId());
        invoice.setBillAddress(consignment.getMovingFromZip());
        invoice.setShipAddress(consignment.getMovingToZip());
        invoice.setDueDate(consignment.getTimeStamp());
        invoice.setStatus("DUE"); //hardcoded for now
        invoice.setSubTotal(0.0f);
        invoice.setTax(0.0f);
        invoice.setTotal(0.0f);
        invoiceDao.save(invoice);
        return new ResponseEntity(invoice, HttpStatus.CREATED);
    }

    @GetMapping("/update/invoice")
    public ResponseEntity updateInvoice(@RequestBody Invoice invoice) {
        invoiceDao.update(invoice);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/update/invoice_records")
    public ResponseEntity addInvRecords(@RequestBody InvoiceRecord invoiceRecord) {
        invoiceRecordDao.save(invoiceRecord);
        return new ResponseEntity(HttpStatus.OK);
    }
}
