package com.amissible.movingsolutions.controller;

import com.amissible.movingsolutions.service.Services;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/ms")
public class Controller { // Add tested and verified code only

    @Autowired
    Services services;

    @RequestMapping("/setup")
    public void setup(){
        services.setup();
    }
}
