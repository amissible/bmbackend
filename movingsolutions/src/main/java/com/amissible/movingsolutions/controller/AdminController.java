package com.amissible.movingsolutions.controller;

import com.amissible.movingsolutions.dao.InvoiceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    InvoiceDao invoiceDao;

    //until we have a format, we will just return invoice
    @GetMapping("/generate_report/invoice")
    public ResponseEntity generateInvoice(Long consignmentId) {
        return new ResponseEntity(invoiceDao.getByConsignmentId(consignmentId), HttpStatus.OK);
    }

    //testing purposes only
    @GetMapping("/view/all_invoices")
    public ResponseEntity getInvoice() {
        return new ResponseEntity(invoiceDao.getAll(), HttpStatus.OK);
    }
}
