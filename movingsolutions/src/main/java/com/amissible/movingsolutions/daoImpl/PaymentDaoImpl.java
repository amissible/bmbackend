package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.PaymentDao;
import com.amissible.movingsolutions.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentDaoImpl implements PaymentDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_payment";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`user_id` bigint unsigned NOT NULL,`consignment_id` bigint unsigned NOT NULL,`txn_id` varchar(50) NOT NULL,`txn_amount` float NOT NULL DEFAULT '0',`txn_status` varchar(20) NOT NULL DEFAULT 'FLAG',`txn_time_stamp` varchar(50) NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByUserId = "SELECT * FROM "+tableName+" WHERE user_id=?";
    private final String getAllByConsignmentId = "SELECT * FROM "+tableName+" WHERE consignment_id=?";
    private final String getAllByTxnStatus = "SELECT * FROM "+tableName+" WHERE txn_status=?";
    private final String getAllByTxnTimeStamp = "SELECT * FROM "+tableName+" WHERE txn_time_stamp LIKE ?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String update = "UPDATE "+tableName+" SET user_id=?,consignment_id=?,txn_id=?,txn_amount=?,txn_status=?,txn_time_stamp=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(user_id,consignment_id,txn_id,txn_amount,txn_status,txn_time_stamp) VALUES(?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByUserId = "DELETE FROM "+tableName+" WHERE user_id=?";
    private final String deleteByConsignmentId = "DELETE FROM "+tableName+" WHERE consignment_id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Payment> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Payment(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
    }

    public List<Payment> getAllByUserId(Long userId){
        return jdbcTemplate.query(getAllByUserId,new Object[]{userId},(rs, rowNum) -> new Payment(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
    }

    public List<Payment> getAllByConsignmentId(Long consignmentId){
        return jdbcTemplate.query(getAllByConsignmentId,new Object[]{consignmentId},(rs, rowNum) -> new Payment(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
    }

    public List<Payment> getAllByTxnStatus(String txnStatus){
        return jdbcTemplate.query(getAllByTxnStatus,new Object[]{txnStatus},(rs, rowNum) -> new Payment(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
    }

    public List<Payment> getAllByTxnTimeStamp(String txnTimeStamp){
        return jdbcTemplate.query(getAllByTxnTimeStamp,new Object[]{"%"+txnTimeStamp+"%"},(rs, rowNum) -> new Payment(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
    }

    public Payment getById(Long id){
        List<Payment> payments = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new Payment(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getString("txn_id"),rs.getFloat("txn_amount"),rs.getString("txn_status"),rs.getString("txn_time_stamp")));
        if(payments.size() == 0)
            return null;
        return payments.get(0);
    }

    public void update(Payment payment){
        jdbcTemplate.update(update,payment.getUserId(),payment.getConsignmentId(),payment.getTxnId(),payment.getTxnAmount(),payment.getTxnStatus(),payment.getTxnTimeStamp(),payment.getId());
    }

    public void save(Payment payment){
        jdbcTemplate.update(save,payment.getUserId(),payment.getConsignmentId(),payment.getTxnId(),payment.getTxnAmount(),payment.getTxnStatus(),payment.getTxnTimeStamp());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByUserId(Long userId){
        jdbcTemplate.update(deleteByUserId, userId);
    }

    public void deleteByConsignmentId(Long consignmentId){
        jdbcTemplate.update(deleteByConsignmentId, consignmentId);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
