package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.ComplaintDao;
import com.amissible.movingsolutions.model.Complaint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComplaintDaoImpl implements ComplaintDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_complaint";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`consignment_id` bigint unsigned NOT NULL,`summary` varchar(255) NOT NULL,`details` text NOT NULL,`resolution` text NOT NULL,`status` varchar(20) NOT NULL,`time_stamp` varchar(50) NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByStatus = "SELECT * FROM "+tableName+" WHERE status=?";
    private final String getAllByTimeStamp = "SELECT * FROM "+tableName+" WHERE time_stamp LIKE ?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getByConsignmentId = "SELECT * FROM "+tableName+" WHERE consignment_id=?";
    private final String update = "UPDATE "+tableName+" SET consignment_id=?,summary=?,details=?,resolution=?,status=?,time_stamp=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(consignment_id,summary,details,resolution,status,time_stamp) VALUES(?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByConsignmentId = "DELETE FROM "+tableName+" WHERE consignment_id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Complaint> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Complaint(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("summary"),rs.getString("details"),rs.getString("resolution"),rs.getString("status"),rs.getString("time_stamp")));
    }

    public List<Complaint> getAllByStatus(String status){
        return jdbcTemplate.query(getAllByStatus,new Object[]{status},(rs, rowNum) -> new Complaint(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("summary"),rs.getString("details"),rs.getString("resolution"),rs.getString("status"),rs.getString("time_stamp")));
    }

    public List<Complaint> getAllByTimeStamp(String timeStamp){
        return jdbcTemplate.query(getAllByTimeStamp,new Object[]{"%"+timeStamp+"%"},(rs, rowNum) -> new Complaint(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("summary"),rs.getString("details"),rs.getString("resolution"),rs.getString("status"),rs.getString("time_stamp")));
    }

    public Complaint getById(Long id){
        List<Complaint> complaints = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new Complaint(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("summary"),rs.getString("details"),rs.getString("resolution"),rs.getString("status"),rs.getString("time_stamp")));
        if(complaints.size() == 0)
            return null;
        return complaints.get(0);
    }

    public Complaint getByConsignmentId(Long consignmentId){
        List<Complaint> complaints = jdbcTemplate.query(getByConsignmentId,new Object[]{consignmentId},(rs, rowNum) -> new Complaint(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("summary"),rs.getString("details"),rs.getString("resolution"),rs.getString("status"),rs.getString("time_stamp")));
        if(complaints.size() == 0)
            return null;
        return complaints.get(0);
    }

    public void update(Complaint complaint){
        jdbcTemplate.update(update,complaint.getConsignmentId(),complaint.getSummary(),complaint.getDetails(),complaint.getResolution(),complaint.getStatus(),complaint.getTimeStamp(),complaint.getId());
    }

    public void save(Complaint complaint){
        jdbcTemplate.update(save,complaint.getConsignmentId(),complaint.getSummary(),complaint.getDetails(),complaint.getResolution(),complaint.getStatus(),complaint.getTimeStamp());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByConsignmentId(Long consignmentId){
        jdbcTemplate.update(deleteByConsignmentId, consignmentId);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
