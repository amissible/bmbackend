package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.ConsignmentDao;
import com.amissible.movingsolutions.model.Consignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsignmentDaoImpl implements ConsignmentDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_consignment";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`user_id` bigint unsigned NOT NULL,`moving_from_zip` varchar(10) NOT NULL,`moving_to_zip` varchar(10) NOT NULL,`moving_from` varchar(150) NOT NULL,`moving_to` varchar(150) NOT NULL,`moving_date` varchar(10) NOT NULL,`service_type` varchar(50) NOT NULL,`time_stamp` varchar(25) NOT NULL,`status` varchar(25) NOT NULL,`status_log` text NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByUserId = "SELECT * FROM "+tableName+" WHERE user_id=?";
    private final String getAllByMovingDate = "SELECT * FROM "+tableName+" WHERE moving_date=?";
    private final String getAllByServiceType = "SELECT * FROM "+tableName+" WHERE service_type=?";
    private final String getAllByStatus = "SELECT * FROM "+tableName+" WHERE status=?";
    private final String getAllByTimeStamp = "SELECT * FROM "+tableName+" WHERE time_stamp LIKE ?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String update = "UPDATE "+tableName+" SET user_id=?,moving_from_zip=?,moving_to_zip=?,moving_from=?,moving_to=?,moving_date=?,service_type=?,time_stamp=?,status=?,status_log=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(user_id,moving_from_zip,moving_to_zip,moving_from,moving_to,moving_date,service_type,time_stamp,status,status_log) VALUES(?,?,?,?,?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByUserId = "DELETE FROM "+tableName+" WHERE user_id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Consignment> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Consignment(rs.getLong("id"),rs.getLong("user_id"),rs.getString("moving_from_zip"),rs.getString("moving_to_zip"),rs.getString("moving_from"),rs.getString("moving_to"),rs.getString("moving_date"),rs.getString("service_type"),rs.getString("time_stamp"),rs.getString("status"),rs.getString("status_log")));
    }

    public List<Consignment> getAllByUserId(Long userId){
        return jdbcTemplate.query(getAllByUserId,new Object[]{userId},(rs, rowNum) -> new Consignment(rs.getLong("id"),rs.getLong("user_id"),rs.getString("moving_from_zip"),rs.getString("moving_to_zip"),rs.getString("moving_from"),rs.getString("moving_to"),rs.getString("moving_date"),rs.getString("service_type"),rs.getString("time_stamp"),rs.getString("status"),rs.getString("status_log")));
    }

    public List<Consignment> getAllByMovingDate(String movingDate){
        return jdbcTemplate.query(getAllByMovingDate,new Object[]{movingDate},(rs, rowNum) -> new Consignment(rs.getLong("id"),rs.getLong("user_id"),rs.getString("moving_from_zip"),rs.getString("moving_to_zip"),rs.getString("moving_from"),rs.getString("moving_to"),rs.getString("moving_date"),rs.getString("service_type"),rs.getString("time_stamp"),rs.getString("status"),rs.getString("status_log")));
    }

    public List<Consignment> getAllByServiceType(String serviceType){
        return jdbcTemplate.query(getAllByServiceType,new Object[]{serviceType},(rs, rowNum) -> new Consignment(rs.getLong("id"),rs.getLong("user_id"),rs.getString("moving_from_zip"),rs.getString("moving_to_zip"),rs.getString("moving_from"),rs.getString("moving_to"),rs.getString("moving_date"),rs.getString("service_type"),rs.getString("time_stamp"),rs.getString("status"),rs.getString("status_log")));
    }

    public List<Consignment> getAllByStatus(String status){
        return jdbcTemplate.query(getAllByStatus,new Object[]{status},(rs, rowNum) -> new Consignment(rs.getLong("id"),rs.getLong("user_id"),rs.getString("moving_from_zip"),rs.getString("moving_to_zip"),rs.getString("moving_from"),rs.getString("moving_to"),rs.getString("moving_date"),rs.getString("service_type"),rs.getString("time_stamp"),rs.getString("status"),rs.getString("status_log")));
    }

    public List<Consignment> getAllByTimeStamp(String timeStamp){
        return jdbcTemplate.query(getAllByTimeStamp,new Object[]{"%"+timeStamp+"%"},(rs, rowNum) -> new Consignment(rs.getLong("id"),rs.getLong("user_id"),rs.getString("moving_from_zip"),rs.getString("moving_to_zip"),rs.getString("moving_from"),rs.getString("moving_to"),rs.getString("moving_date"),rs.getString("service_type"),rs.getString("time_stamp"),rs.getString("status"),rs.getString("status_log")));
    }

    public Consignment getById(Long id){
        List<Consignment> consignments = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new Consignment(rs.getLong("id"),rs.getLong("user_id"),rs.getString("moving_from_zip"),rs.getString("moving_to_zip"),rs.getString("moving_from"),rs.getString("moving_to"),rs.getString("moving_date"),rs.getString("service_type"),rs.getString("time_stamp"),rs.getString("status"),rs.getString("status_log")));
        if(consignments.size() == 0)
            return null;
        return consignments.get(0);
    }

    @Override
    public Long getUserId(Long consignmentId) {
        List<Long> idList = jdbcTemplate.query(getById, new Object[]{consignmentId},(rs, rowNum) -> (rs.getLong("user_id")));
        if(idList.size() == 0)
            return null;
        return idList.get(0);
    }

    public void update(Consignment consignment){
        jdbcTemplate.update(update,consignment.getUserId(),consignment.getMovingFromZip(),consignment.getMovingToZip(),consignment.getMovingFrom(),consignment.getMovingTo(),consignment.getMovingDate(),consignment.getServiceType(),consignment.getTimeStamp(),consignment.getStatus(),consignment.getStatusLog(),consignment.getId());
    }

    public void save(Consignment consignment){
        jdbcTemplate.update(save,consignment.getUserId(),consignment.getMovingFromZip(),consignment.getMovingToZip(),consignment.getMovingFrom(),consignment.getMovingTo(),consignment.getMovingDate(),consignment.getServiceType(),consignment.getTimeStamp(),consignment.getStatus(),consignment.getStatusLog());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByUserId(Long userId){
        jdbcTemplate.update(deleteByUserId, userId);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }

    public String setStatus(Long id){
        final String approve = "update am_ms_consignment set status = \"Approved\"\n where id = "+ id + " AND status = \"WatingApproval\"";
        jdbcTemplate.update(approve);
        return "Approved";

    }
}
