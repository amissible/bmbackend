package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.InvoiceRecordDao;
import com.amissible.movingsolutions.model.InvoiceRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceRecordDaoImpl implements InvoiceRecordDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_invoice_record";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`invoice_id` bigint unsigned NOT NULL,`description` varchar(255) NOT NULL,`quantity` int NOT NULL DEFAULT '0',`unit_price` float NOT NULL DEFAULT '0',`amount` float NOT NULL DEFAULT '0')";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByInvoiceId = "SELECT * FROM "+tableName+" WHERE invoice_id=?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String update = "UPDATE "+tableName+" SET invoice_id=?,description=?,quantity=?,unit_price=?,amount=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(invoice_id,description,quantity,unit_price,amount) VALUES(?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByInvoiceId = "DELETE FROM "+tableName+" WHERE invoice_id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<InvoiceRecord> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new InvoiceRecord(rs.getLong("id"),rs.getLong("invoice_id"),rs.getString("description"),rs.getInt("quantity"),rs.getFloat("unit_price"),rs.getFloat("amount")));
    }

    public List<InvoiceRecord> getAllByInvoiceId(Long invoiceId){
        return jdbcTemplate.query(getAllByInvoiceId,new Object[]{invoiceId},(rs, rowNum) -> new InvoiceRecord(rs.getLong("id"),rs.getLong("invoice_id"),rs.getString("description"),rs.getInt("quantity"),rs.getFloat("unit_price"),rs.getFloat("amount")));
    }

    public InvoiceRecord getById(Long id){
        List<InvoiceRecord> invoiceRecords = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new InvoiceRecord(rs.getLong("id"),rs.getLong("invoice_id"),rs.getString("description"),rs.getInt("quantity"),rs.getFloat("unit_price"),rs.getFloat("amount")));
        if(invoiceRecords.size() == 0)
            return null;
        return invoiceRecords.get(0);
    }

    public void update(InvoiceRecord invoiceRecord){
        jdbcTemplate.update(update,invoiceRecord.getInvoiceId(),invoiceRecord.getDescription(),invoiceRecord.getQuantity(),invoiceRecord.getUnitPrice(),invoiceRecord.getAmount(),invoiceRecord.getId());
    }

    public void save(InvoiceRecord invoiceRecord){
        jdbcTemplate.update(save,invoiceRecord.getInvoiceId(),invoiceRecord.getDescription(),invoiceRecord.getQuantity(),invoiceRecord.getUnitPrice(),invoiceRecord.getAmount());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByInvoiceId(Long invoiceId){
        jdbcTemplate.update(deleteByInvoiceId, invoiceId);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
