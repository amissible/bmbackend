package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.SettingDao;
import com.amissible.movingsolutions.model.Setting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SettingDaoImpl implements SettingDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_setting";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`key` varchar(255) NOT NULL,`value` varchar(255) NOT NULL)";
    private final String getAllSettings = "SELECT * FROM "+tableName;
    private final String getSettingById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getSettingByKey = "SELECT * FROM "+tableName+" WHERE `key`=?";
    private final String update = "UPDATE "+tableName+" SET `key`=?,`value`=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(`key`,`value`) VALUES(?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    private Map<String,String> settings = new HashMap<>();

    public String get(String key){
        return settings.get(key);
    }

    public void refresh(){
        List<Setting> settingList = getAllSettings();
        for(Setting s:settingList)
            settings.put(s.getKey(),s.getValue());
    }

    public void createTable(){
        jdbcTemplate.update(create);
        refresh();
        if(settings.get("TimeZone") == null) save(new Setting("TimeZone","Asia/Kolkata"));
        if(settings.get("DateFormat") == null) save(new Setting("DateFormat","yyyy-MM-dd"));
        if(settings.get("TimeFormat") == null) save(new Setting("TimeFormat","HH:mm:ss"));
        if(settings.get("DateTimeFormat") == null) save(new Setting("DateTimeFormat","yyyy-MM-dd HH:mm:ss"));
        if(settings.get("Currency") == null) save(new Setting("Currency","USD"));
        if(settings.get("PaymentAlias") == null) save(new Setting("PaymentAlias","MOVSOL"));
        if(settings.get("PaymentPath") == null) save(new Setting("PaymentPath","https://www.amissible.com:8083/index"));
        if(settings.get("SuccessPayPath") == null) save(new Setting("SuccessPayPath","http://localhost:4200/payment"));
        if(settings.get("FailurePayPath") == null) save(new Setting("FailurePayPath","http://localhost:4200/consignment"));
        if(settings.get("ExportPath") == null) save(new Setting("ExportPath","/app/export/movsol/"));
    }

    public List<Setting> getAllSettings(){
        return jdbcTemplate.query(getAllSettings,new Object[]{},(rs,rowNum) -> new Setting(rs.getLong("id"),rs.getString("key"),rs.getString("value")));
    }

    public Setting getSettingById(Long id){
        List<Setting> settings = jdbcTemplate.query(getSettingById,new Object[]{id},(rs, rowNum) -> new Setting(rs.getLong("id"),rs.getString("key"),rs.getString("value")));
        if(settings.size() == 0)
            return null;
        return settings.get(0);
    }

    public Setting getSettingByKey(String key){
        List<Setting> settings = jdbcTemplate.query(getSettingByKey,new Object[]{key},(rs, rowNum) -> new Setting(rs.getLong("id"),rs.getString("key"),rs.getString("value")));
        if(settings.size() == 0)
            return null;
        return settings.get(0);
    }

    public void update(Setting setting){
        jdbcTemplate.update(update, setting.getKey(),setting.getValue(),setting.getId());
        refresh();
    }

    public void save(Setting setting){
        jdbcTemplate.update(save, setting.getKey(),setting.getValue());
        refresh();
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
        refresh();
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
        refresh();
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
