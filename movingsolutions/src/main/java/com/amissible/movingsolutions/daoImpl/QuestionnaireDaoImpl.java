package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.QuestionnaireDao;
import com.amissible.movingsolutions.model.Payment;
import com.amissible.movingsolutions.model.Questionnaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionnaireDaoImpl implements QuestionnaireDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_questionnaire";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`consignment_id` bigint unsigned NOT NULL,`question_id` bigint unsigned NOT NULL,`response` varchar(255) NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByConsignmentId = "SELECT * FROM "+tableName+" WHERE consignment_id=?";
    private final String getAllByQuestionId = "SELECT * FROM "+tableName+" WHERE question_id=?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String get = "SELECT * FROM "+tableName+" WHERE consignment_id=? AND question_id=?";
    private final String update = "UPDATE "+tableName+" SET consignment_id=?,question_id=?,response=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(consignment_id,question_id,response) VALUES(?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByConsignmentId = "DELETE FROM "+tableName+" WHERE consignment_id=?";
    private final String deleteByQuestionId = "DELETE FROM "+tableName+" WHERE question_id=?";
    private final String del = "DELETE FROM "+tableName+" WHERE consignment_id=? AND question_id";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Questionnaire> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Questionnaire(rs.getLong("id"),rs.getLong("consignment_id"),rs.getLong("question_id"),rs.getString("response")));
    }

    public List<Questionnaire> getAllByConsignmentId(Long consignmentId){
        return jdbcTemplate.query(getAllByConsignmentId,new Object[]{consignmentId},(rs, rowNum) -> new Questionnaire(rs.getLong("id"),rs.getLong("consignment_id"),rs.getLong("question_id"),rs.getString("response")));
    }

    public List<Questionnaire> getAllByQuestionId(Long questionId){
        return jdbcTemplate.query(getAllByQuestionId,new Object[]{questionId},(rs, rowNum) -> new Questionnaire(rs.getLong("id"),rs.getLong("consignment_id"),rs.getLong("question_id"),rs.getString("response")));
    }

    public Questionnaire getById(Long id){
        List<Questionnaire> questionnaires = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new Questionnaire(rs.getLong("id"),rs.getLong("consignment_id"),rs.getLong("question_id"),rs.getString("response")));
        if(questionnaires.size() == 0)
            return null;
        return questionnaires.get(0);
    }

    public Questionnaire get(Long consignmentId,Long questionId){
        List<Questionnaire> questionnaires = jdbcTemplate.query(get,new Object[]{consignmentId,questionId},(rs, rowNum) -> new Questionnaire(rs.getLong("id"),rs.getLong("consignment_id"),rs.getLong("question_id"),rs.getString("response")));
        if(questionnaires.size() == 0)
            return null;
        return questionnaires.get(0);
    }

    public void update(Questionnaire questionnaire){
        jdbcTemplate.update(update,questionnaire.getConsignmentId(),questionnaire.getQuestionId(),questionnaire.getId());
    }

    public void save(Questionnaire questionnaire){
        jdbcTemplate.update(save,questionnaire.getConsignmentId(),questionnaire.getQuestionId(),questionnaire.getResponse());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByConsignmentId(Long consignmentId){
        jdbcTemplate.update(deleteByConsignmentId, consignmentId);
    }

    public void deleteByQuestionId(Long questionId){
        jdbcTemplate.update(deleteByQuestionId, questionId);
    }

    public void del(Long consignmentId,Long questionId){
        jdbcTemplate.update(del, consignmentId, questionId);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
