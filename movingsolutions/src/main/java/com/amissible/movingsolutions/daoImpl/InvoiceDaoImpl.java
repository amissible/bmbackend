package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.InvoiceDao;
import com.amissible.movingsolutions.model.Invoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceDaoImpl implements InvoiceDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_invoice";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`consignment_id` bigint unsigned NOT NULL,`bill_address` varchar(255) NOT NULL,`ship_address` varchar(255) NOT NULL,`date` varchar(11) NOT NULL,`due_date` varchar(11) NOT NULL,`status` varchar(20) NOT NULL,`sub_total` float NOT NULL DEFAULT '0',`discount` float NOT NULL DEFAULT '0',`tax` float NOT NULL DEFAULT '0',`total` float NOT NULL DEFAULT '0')";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByDate = "SELECT * FROM "+tableName+" WHERE `date` LIKE ?";
    private final String getAllByDueDate = "SELECT * FROM "+tableName+" WHERE due_date LIKE ?";
    private final String getAllByStatus = "SELECT * FROM "+tableName+" WHERE status=?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getByConsignmentId = "SELECT * FROM "+tableName+" WHERE consignment_id=? and status!='INACTIVE'";
    private final String getAllByConsignmentId = "SELECT * FROM "+tableName+" WHERE consignment_id=?";
    private final String update = "UPDATE "+tableName+" SET consignment_id=?,bill_address=?,ship_address=?,`date`=?,due_date=?,status=?,sub_total=?,discount=?,tax=?,total=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(consignment_id,bill_address,ship_address,`date`,due_date,status,sub_total,discount,tax,total) VALUES(?,?,?,?,?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByConsignmentId = "DELETE FROM "+tableName+" WHERE consignment_id=?";
    private final String deleteByStatus = "DELETE FROM "+tableName+" WHERE status=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Invoice> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Invoice(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("bill_address"),rs.getString("ship_address"),rs.getString("date"),rs.getString("due_date"),rs.getString("status"),rs.getFloat("sub_total"),rs.getFloat("discount"),rs.getFloat("tax"),rs.getFloat("total")));
    }

    public List<Invoice> getAllByDate(String date){
        return jdbcTemplate.query(getAllByDate,new Object[]{"%"+date+"%"},(rs, rowNum) -> new Invoice(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("bill_address"),rs.getString("ship_address"),rs.getString("date"),rs.getString("due_date"),rs.getString("status"),rs.getFloat("sub_total"),rs.getFloat("discount"),rs.getFloat("tax"),rs.getFloat("total")));
    }

    public List<Invoice> getAllByDueDate(String dueDate){
        return jdbcTemplate.query(getAllByDueDate,new Object[]{"%"+dueDate+"%"},(rs, rowNum) -> new Invoice(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("bill_address"),rs.getString("ship_address"),rs.getString("date"),rs.getString("due_date"),rs.getString("status"),rs.getFloat("sub_total"),rs.getFloat("discount"),rs.getFloat("tax"),rs.getFloat("total")));
    }

    public List<Invoice> getAllByStatus(String status){
        return jdbcTemplate.query(getAllByStatus,new Object[]{status},(rs, rowNum) -> new Invoice(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("bill_address"),rs.getString("ship_address"),rs.getString("date"),rs.getString("due_date"),rs.getString("status"),rs.getFloat("sub_total"),rs.getFloat("discount"),rs.getFloat("tax"),rs.getFloat("total")));
    }

    public Invoice getById(Long id){
        List<Invoice> invoices = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new Invoice(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("bill_address"),rs.getString("ship_address"),rs.getString("date"),rs.getString("due_date"),rs.getString("status"),rs.getFloat("sub_total"),rs.getFloat("discount"),rs.getFloat("tax"),rs.getFloat("total")));
        if(invoices.size() == 0)
            return null;
        return invoices.get(0);
    }

    public Invoice getByConsignmentId(Long consignmentId){
        List<Invoice> invoices = jdbcTemplate.query(getByConsignmentId,new Object[]{consignmentId},(rs, rowNum) -> new Invoice(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("bill_address"),rs.getString("ship_address"),rs.getString("date"),rs.getString("due_date"),rs.getString("status"),rs.getFloat("sub_total"),rs.getFloat("discount"),rs.getFloat("tax"),rs.getFloat("total")));
        if(invoices.size() == 0)
            return null;
        return invoices.get(0);
    }

    public List<Invoice> getAllByConsignmentId(Long consignmentId){
        return jdbcTemplate.query(getAllByConsignmentId,new Object[]{consignmentId},(rs, rowNum) -> new Invoice(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("bill_address"),rs.getString("ship_address"),rs.getString("date"),rs.getString("due_date"),rs.getString("status"),rs.getFloat("sub_total"),rs.getFloat("discount"),rs.getFloat("tax"),rs.getFloat("total")));
    }

    public void update(Invoice invoice){
        jdbcTemplate.update(update,invoice.getConsignmentId(),invoice.getBillAddress(),invoice.getShipAddress(),invoice.getDate(),invoice.getDueDate(),invoice.getStatus(),invoice.getSubTotal(),invoice.getDiscount(),invoice.getTax(),invoice.getTotal(),invoice.getId());
    }

    public void save(Invoice invoice){
        jdbcTemplate.update(save,invoice.getConsignmentId(),invoice.getBillAddress(),invoice.getShipAddress(),invoice.getDate(),invoice.getDueDate(),invoice.getStatus(),invoice.getSubTotal(),invoice.getDiscount(),invoice.getTax(),invoice.getTotal());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByConsignmentId(Long consignmentId){
        jdbcTemplate.update(deleteByConsignmentId, consignmentId);
    }

    public void deleteByStatus(String status){
        jdbcTemplate.update(deleteByStatus, status);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
