package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.FeedbackDao;
import com.amissible.movingsolutions.model.Feedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedbackDaoImpl implements FeedbackDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_feedback";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`consignment_id` bigint unsigned NOT NULL,`rating` int NOT NULL,`review` varchar(255) NOT NULL,`time_stamp` varchar(50) NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByTimeStamp = "SELECT * FROM "+tableName+" WHERE time_stamp LIKE ?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getByConsignmentId = "SELECT * FROM "+tableName+" WHERE consignment_id=?";
    private final String update = "UPDATE "+tableName+" SET consignment_id=?,rating=?,review=?,time_stamp=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(consignment_id,rating,review,time_stamp) VALUES(?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByConsignmentId = "DELETE FROM "+tableName+" WHERE consignment_id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Feedback> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Feedback(rs.getLong("id"),rs.getLong("consignment_id"),rs.getInt("rating"),rs.getString("review"),rs.getString("time_stamp")));
    }

    public List<Feedback> getAllByTimeStamp(String timeStamp){
        return jdbcTemplate.query(getAllByTimeStamp,new Object[]{"%"+timeStamp+"%"},(rs, rowNum) -> new Feedback(rs.getLong("id"),rs.getLong("consignment_id"),rs.getInt("rating"),rs.getString("review"),rs.getString("time_stamp")));
    }

    public Feedback getById(Long id){
        List<Feedback> feedbacks = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new Feedback(rs.getLong("id"),rs.getLong("consignment_id"),rs.getInt("rating"),rs.getString("review"),rs.getString("time_stamp")));
        if(feedbacks.size() == 0)
            return null;
        return feedbacks.get(0);
    }

    public Feedback getByConsignmentId(Long consignmentId){
        List<Feedback> feedbacks = jdbcTemplate.query(getByConsignmentId,new Object[]{consignmentId},(rs, rowNum) -> new Feedback(rs.getLong("id"),rs.getLong("consignment_id"),rs.getInt("rating"),rs.getString("review"),rs.getString("time_stamp")));
        if(feedbacks.size() == 0)
            return null;
        return feedbacks.get(0);
    }

    public void update(Feedback feedback){
        jdbcTemplate.update(update,feedback.getConsignmentId(),feedback.getRating(),feedback.getReview(),feedback.getTimeStamp(),feedback.getId());
    }

    public void save(Feedback feedback){
        jdbcTemplate.update(save,feedback.getConsignmentId(),feedback.getRating(),feedback.getReview(),feedback.getTimeStamp());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByConsignmentId(Long consignmentId){
        jdbcTemplate.update(deleteByConsignmentId, consignmentId);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
