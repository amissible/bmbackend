package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.UserDao;
import com.amissible.movingsolutions.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDaoImpl implements UserDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_user";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`name` varchar(100) NOT NULL,`email` varchar(100) NOT NULL,`contact` varchar(15) NOT NULL,`password` varchar(100) NOT NULL,`time_zone` varchar(50) NOT NULL,`time_stamp` varchar(25) NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByTimeStamp = "SELECT * FROM "+tableName+" WHERE time_stamp LIKE ?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getByEmail = "SELECT * FROM "+tableName+" WHERE email=?";
    private final String update = "UPDATE "+tableName+" SET name=?,email=?,contact=?,password=?,time_zone=?,time_stamp=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(name,email,contact,password,time_zone,time_stamp) VALUES(?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByEmail = "DELETE FROM "+tableName+" WHERE email=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<User> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new User(rs.getLong("id"),rs.getString("name"),rs.getString("email"),rs.getString("contact"),rs.getString("password"),rs.getString("time_zone"),rs.getString("time_stamp")));
    }

    public List<User> getAllByTimeStamp(String timeStamp){
        return jdbcTemplate.query(getAllByTimeStamp,new Object[]{"%"+timeStamp+"%"},(rs, rowNum) -> new User(rs.getLong("id"),rs.getString("name"),rs.getString("email"),rs.getString("contact"),rs.getString("password"),rs.getString("time_zone"),rs.getString("time_stamp")));
    }

    public User getById(Long id){
        List<User> users = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) ->
                new User(rs.getLong("id"),rs.getString("name"),rs.getString("email"),
                        rs.getString("contact"),rs.getString("password"),
                        rs.getString("time_zone"),rs.getString("time_stamp")));
        if(users.size() == 0)
            return null;
        return users.get(0);
    }

    public User getByEmail(String email){
        List<User> users = jdbcTemplate.query(getByEmail,new Object[]{email},(rs, rowNum) ->
                new User(rs.getLong("id"),rs.getString("name"),rs.getString("email"),
                        rs.getString("contact"),rs.getString("password"),
                        rs.getString("time_zone"),rs.getString("time_stamp")));
        if(users.size() == 0)
            return null;
        return users.get(0);
    }

    public void update(User user){
        jdbcTemplate.update(update,user.getName(),user.getEmail(),user.getContact(),user.getPassword(),user.getTimeZone(),user.getTimeStamp(),user.getId());
    }

    public void save(User user){
        jdbcTemplate.update(save,user.getName(),user.getEmail(),user.getContact(),user.getPassword(),user.getTimeZone(),user.getTimeStamp());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByEmail(String email){
        jdbcTemplate.update(deleteByEmail, email);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
