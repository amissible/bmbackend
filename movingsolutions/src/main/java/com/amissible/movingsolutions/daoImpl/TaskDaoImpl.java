package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.TaskDao;
import com.amissible.movingsolutions.model.Complaint;
import com.amissible.movingsolutions.model.Task;
import com.amissible.movingsolutions.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskDaoImpl implements TaskDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_task";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`user_id` bigint unsigned NOT NULL,`consignment_id` bigint unsigned NOT NULL,`employee_id` bigint unsigned NOT NULL,`instructions` varchar(255) NOT NULL,`notes` varchar(255) NOT NULL,`status` varchar(20) NOT NULL,`issued_on` varchar(25) NOT NULL,`dated_to` varchar(25) NOT NULL,`closed_on` varchar(25) NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getByDatedTo = "SELECT * FROM "+tableName+" WHERE employee_id=? AND dated_to LIKE ?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getByUserId = "SELECT * FROM "+tableName+" WHERE user_id=?";
    private final String getByConsignmentId = "SELECT * FROM "+tableName+" WHERE consignment_id=?";
    private final String getByEmployeeId = "SELECT * FROM "+tableName+" WHERE employee_id=?";
    private final String getPendingByEmployeeId = "SELECT * FROM "+tableName+" WHERE employee_id=? AND status IN ('NEW','EXECUTING')";
    private final String getByStatus = "SELECT * FROM "+tableName+" WHERE status=?";
    private final String update = "UPDATE "+tableName+" SET user_id=?,consignment_id=?,employee_id=?,instructions=?,notes=?,status=?,issued_on=?,dated_to=?,closed_on=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(user_id,consignment_id,employee_id,instructions,notes,status,issued_on,dated_to,closed_on) VALUES(?,?,?,?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByUserId = "DELETE FROM "+tableName+" WHERE user_id=?";
    private final String deleteByConsignmentId = "DELETE FROM "+tableName+" WHERE consignment_id=?";
    private final String deleteByEmployeeId = "DELETE FROM "+tableName+" WHERE employee_id=?";
    private final String deleteByStatus = "DELETE FROM "+tableName+" WHERE status=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Task> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Task(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getLong("employee_id"),rs.getString("instructions"),rs.getString("notes"),rs.getString("status"),rs.getString("issued_on"),rs.getString("dated_to"),rs.getString("closed_on")));
    }

    public List<Task> getByDatedTo(Long employeeId, String datedTo){
        return jdbcTemplate.query(getByDatedTo,new Object[]{employeeId,"%"+datedTo+"%"},(rs, rowNum) -> new Task(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getLong("employee_id"),rs.getString("instructions"),rs.getString("notes"),rs.getString("status"),rs.getString("issued_on"),rs.getString("dated_to"),rs.getString("closed_on")));
    }

    public Task getById(Long userId){
        List<Task> tasks = jdbcTemplate.query(getById,new Object[]{userId},(rs, rowNum) -> new Task(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getLong("employee_id"),rs.getString("instructions"),rs.getString("notes"),rs.getString("status"),rs.getString("issued_on"),rs.getString("dated_to"),rs.getString("closed_on")));
        if(tasks.size() == 0)
            return null;
        else
            return tasks.get(0);
    }

    public List<Task> getByUserId(Long userId){
        return jdbcTemplate.query(getByUserId,new Object[]{userId},(rs, rowNum) -> new Task(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getLong("employee_id"),rs.getString("instructions"),rs.getString("notes"),rs.getString("status"),rs.getString("issued_on"),rs.getString("dated_to"),rs.getString("closed_on")));
    }

    public List<Task> getByConsignmentId(Long consignmentId){
        return jdbcTemplate.query(getByConsignmentId,new Object[]{consignmentId},(rs, rowNum) -> new Task(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getLong("employee_id"),rs.getString("instructions"),rs.getString("notes"),rs.getString("status"),rs.getString("issued_on"),rs.getString("dated_to"),rs.getString("closed_on")));
    }

    public List<Task> getByEmployeeId(Long employeeId){
        return jdbcTemplate.query(getByEmployeeId,new Object[]{employeeId},(rs, rowNum) -> new Task(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getLong("employee_id"),rs.getString("instructions"),rs.getString("notes"),rs.getString("status"),rs.getString("issued_on"),rs.getString("dated_to"),rs.getString("closed_on")));
    }

    public List<Task> getPendingByEmployeeId(Long employeeId){
        return jdbcTemplate.query(getPendingByEmployeeId,new Object[]{employeeId},(rs, rowNum) -> new Task(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getLong("employee_id"),rs.getString("instructions"),rs.getString("notes"),rs.getString("status"),rs.getString("issued_on"),rs.getString("dated_to"),rs.getString("closed_on")));
    }

    public List<Task> getByStatus(String status){
        return jdbcTemplate.query(getByStatus,new Object[]{status},(rs, rowNum) -> new Task(rs.getLong("id"),rs.getLong("user_id"),rs.getLong("consignment_id"),rs.getLong("employee_id"),rs.getString("instructions"),rs.getString("notes"),rs.getString("status"),rs.getString("issued_on"),rs.getString("dated_to"),rs.getString("closed_on")));
    }

    public void update(Task task){
        jdbcTemplate.update(update,task.getUserId(),task.getConsignmentId(),task.getEmployeeId(),task.getInstructions(),task.getNotes(),task.getStatus(),task.getIssuedOn(),task.getDatedTo(),task.getClosedOn(),task.getId());
    }

    public void save(Task task){
        jdbcTemplate.update(save,task.getUserId(),task.getConsignmentId(),task.getEmployeeId(),task.getInstructions(),task.getNotes(),task.getStatus(),task.getIssuedOn(),task.getDatedTo(),task.getClosedOn());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByUserId(Long user_id){
        jdbcTemplate.update(deleteByUserId, user_id);
    }

    public void deleteByConsignmentId(Long consignment_id){
        jdbcTemplate.update(deleteByConsignmentId, consignment_id);
    }

    public void deleteByEmployeeId(Long employee_id){
        jdbcTemplate.update(deleteByEmployeeId, employee_id);
    }

    public void deleteByStatus(String status){
        jdbcTemplate.update(deleteByStatus, status);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
