package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.EmployeeDao;
import com.amissible.movingsolutions.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeDaoImpl implements EmployeeDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_employee";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`name` varchar(100) NOT NULL,`email` varchar(100) NOT NULL,`contact` varchar(15) NOT NULL,`password` varchar(100) NOT NULL,`role` varchar(25) NOT NULL,`time_stamp` varchar(25) NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByRole = "SELECT * FROM "+tableName+" WHERE role=?";
    private final String getAllByTimeStamp = "SELECT * FROM "+tableName+" WHERE time_stamp LIKE ?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getByEmail = "SELECT * FROM "+tableName+" WHERE email=?";
    private final String update = "UPDATE "+tableName+" SET name=?,email=?,contact=?,password=?,role=?,time_stamp=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(name,email,contact,password,role,time_stamp) VALUES(?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByEmail = "DELETE FROM "+tableName+" WHERE email=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Employee> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Employee(rs.getLong("id"),rs.getString("name"),rs.getString("email"),rs.getString("contact"),rs.getString("password"),rs.getString("role"),rs.getString("time_stamp")));
    }

    public List<Employee> getAllByRole(String role){
        return jdbcTemplate.query(getAllByRole,new Object[]{role},(rs, rowNum) -> new Employee(rs.getLong("id"),rs.getString("name"),rs.getString("email"),rs.getString("contact"),rs.getString("password"),rs.getString("role"),rs.getString("time_stamp")));
    }

    public List<Employee> getAllByTimeStamp(String timeStamp){
        return jdbcTemplate.query(getAllByTimeStamp,new Object[]{"%"+timeStamp+"%"},(rs, rowNum) -> new Employee(rs.getLong("id"),rs.getString("name"),rs.getString("email"),rs.getString("contact"),rs.getString("password"),rs.getString("role"),rs.getString("time_stamp")));
    }

    public Employee getById(Long id){
        List<Employee> employees = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new Employee(rs.getLong("id"),rs.getString("name"),rs.getString("email"),rs.getString("contact"),rs.getString("password"),rs.getString("role"),rs.getString("time_stamp")));
        if(employees.size() == 0)
            return null;
        return employees.get(0);
    }

    public Employee getByEmail(String email){
        List<Employee> employees = jdbcTemplate.query(getByEmail,new Object[]{email},(rs, rowNum) -> new Employee(rs.getLong("id"),rs.getString("name"),rs.getString("email"),rs.getString("contact"),rs.getString("password"),rs.getString("role"),rs.getString("time_stamp")));
        if(employees.size() == 0)
            return null;
        return employees.get(0);
    }

    public void update(Employee employee){
        jdbcTemplate.update(update,employee.getName(),employee.getEmail(),employee.getContact(),employee.getPassword(),employee.getRole(),employee.getTimeStamp(),employee.getId());
    }

    public void save(Employee employee){
        jdbcTemplate.update(save,employee.getName(),employee.getEmail(),employee.getContact(),employee.getPassword(),employee.getRole(),employee.getTimeStamp());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByEmail(String email){
        jdbcTemplate.update(deleteByEmail, email);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
