package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.QuestionDao;
import com.amissible.movingsolutions.model.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionDaoImpl implements QuestionDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_question";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`question` varchar(255) NOT NULL,`status` varchar(10) NOT NULL DEFAULT 'ACTIVE')";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByStatus = "SELECT * FROM "+tableName+" WHERE status=?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String update = "UPDATE "+tableName+" SET question=?,status=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(question,status) VALUES(?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByStatus = "DELETE FROM "+tableName+" WHERE status=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Question> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Question(rs.getLong("id"),rs.getString("question"),rs.getString("status")));
    }

    public List<Question> getAllByStatus(String status){
        return jdbcTemplate.query(getAllByStatus,new Object[]{status},(rs, rowNum) -> new Question(rs.getLong("id"),rs.getString("question"),rs.getString("status")));
    }

    public Question getById(Long id){
        List<Question> questions = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new Question(rs.getLong("id"),rs.getString("question"),rs.getString("status")));
        if(questions.size() == 0)
            return null;
        return questions.get(0);
    }

    public void update(Question question){
        jdbcTemplate.update(update,question.getQuestion(),question.getStatus(),question.getId());
    }

    public void save(Question question){
        jdbcTemplate.update(save,question.getQuestion(),question.getStatus());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByStatus(String status){
        jdbcTemplate.update(deleteByStatus, status);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
