package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.ContractDao;
import com.amissible.movingsolutions.model.Contract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContractDaoImpl implements ContractDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_contract";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`consignment_id` bigint unsigned NOT NULL,`date` varchar(11) NOT NULL,`statement` text NOT NULL,`first_party` text NOT NULL,`second_party` text NOT NULL,`other_terms` text NOT NULL,`url` varchar(150) NOT NULL,`signed_url` varchar(150) NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByDate = "SELECT * FROM "+tableName+" WHERE `date` LIKE ?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String getByConsignmentId = "SELECT * FROM "+tableName+" WHERE consignment_id=?";
    private final String update = "UPDATE "+tableName+" SET consignment_id=?,date=?,statement=?,first_party=?,second_party=?,other_terms=?,url=?,signed_url=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(consignment_id,date,statement,first_party,second_party,other_terms,url,signed_url) VALUES(?,?,?,?,?,?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByConsignmentId = "DELETE FROM "+tableName+" WHERE consignment_id=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<Contract> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new Contract(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("date"),rs.getString("statement"),rs.getString("first_party"),rs.getString("second_party"),rs.getString("other_terms"),rs.getString("url"),rs.getString("signed_url")));
    }

    public List<Contract> getAllByDate(String date){
        return jdbcTemplate.query(getAllByDate,new Object[]{"%"+date+"%"},(rs, rowNum) -> new Contract(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("date"),rs.getString("statement"),rs.getString("first_party"),rs.getString("second_party"),rs.getString("other_terms"),rs.getString("url"),rs.getString("signed_url")));
    }

    public Contract getById(Long id){
        List<Contract> contracts = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new Contract(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("date"),rs.getString("statement"),rs.getString("first_party"),rs.getString("second_party"),rs.getString("other_terms"),rs.getString("url"),rs.getString("signed_url")));
        if(contracts.size() == 0)
            return null;
        return contracts.get(0);
    }

    public Contract getByConsignmentId(Long consignmentId){
        List<Contract> contracts = jdbcTemplate.query(getByConsignmentId,new Object[]{consignmentId},(rs, rowNum) -> new Contract(rs.getLong("id"),rs.getLong("consignment_id"),rs.getString("date"),rs.getString("statement"),rs.getString("first_party"),rs.getString("second_party"),rs.getString("other_terms"),rs.getString("url"),rs.getString("signed_url")));
        if(contracts.size() == 0)
            return null;
        return contracts.get(0);
    }

    public void update(Contract contract){
        jdbcTemplate.update(update,contract.getConsignmentId(),contract.getDate(),contract.getStatement(),contract.getFirstParty(),contract.getSecondParty(),contract.getOtherTerms(),contract.getUrl(),contract.getSignedUrl(),contract.getId());
    }

    public void save(Contract contract){
        jdbcTemplate.update(save,contract.getConsignmentId(),contract.getDate(),contract.getStatement(),contract.getFirstParty(),contract.getSecondParty(),contract.getOtherTerms(),contract.getUrl(),contract.getSignedUrl());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByConsignmentId(Long consignmentId){
        jdbcTemplate.update(deleteByConsignmentId, consignmentId);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
