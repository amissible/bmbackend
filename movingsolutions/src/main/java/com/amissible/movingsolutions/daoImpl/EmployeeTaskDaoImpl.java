package com.amissible.movingsolutions.daoImpl;

import com.amissible.movingsolutions.dao.EmployeeTaskDao;
import com.amissible.movingsolutions.model.Employee;
import com.amissible.movingsolutions.model.EmployeeTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeTaskDaoImpl implements EmployeeTaskDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String tableName = "am_ms_employee_task";
    private final String create = "CREATE TABLE IF NOT EXISTS `"+tableName+"` (`id` bigint unsigned PRIMARY KEY AUTO_INCREMENT,`employee_id` bigint unsigned NOT NULL,`consignment_id` bigint unsigned NOT NULL,`status` varchar(20) NOT NULL)";
    private final String getAll = "SELECT * FROM "+tableName;
    private final String getAllByEmployeeId = "SELECT * FROM "+tableName+" WHERE employee_id=?";
    private final String getAllByConsignmentId = "SELECT * FROM "+tableName+" WHERE consignment_id=?";
    private final String getAllByStatus = "SELECT * FROM "+tableName+" WHERE status=?";
    private final String getAllByEC = "SELECT * FROM "+tableName+" WHERE  employee_id=? AND consignment_id=?";
    private final String getAllByES = "SELECT * FROM "+tableName+" WHERE  employee_id=? AND status=?";
    private final String getAllByCS = "SELECT * FROM "+tableName+" WHERE  consignment_id=? AND status=?";
    private final String getAllByECS = "SELECT * FROM "+tableName+" WHERE  employee_id=? AND consignment_id=? AND status=?";
    private final String getById = "SELECT * FROM "+tableName+" WHERE id=?";
    private final String update = "UPDATE "+tableName+" SET employee_id=?,consignment_id=?,status=? WHERE id=?";
    private final String save = "INSERT INTO "+tableName+"(employee_id,consignment_id,status) VALUES(?,?,?)";
    private final String delete = "DELETE FROM "+tableName+" WHERE id=?";
    private final String deleteByEmployeeId = "DELETE FROM "+tableName+" WHERE employee_id=?";
    private final String deleteByConsignmentId = "DELETE FROM "+tableName+" WHERE consignment_id=?";
    private final String deleteByStatus = "DELETE FROM "+tableName+" WHERE status=?";
    private final String deleteAll = "DELETE FROM "+tableName+" WHERE 1";
    private final String dropTable = "DROP TABLE "+tableName;

    public void createTable(){
        jdbcTemplate.update(create);
    }

    public List<EmployeeTask> getAll(){
        return jdbcTemplate.query(getAll,new Object[]{},(rs, rowNum) -> new EmployeeTask(rs.getLong("id"),rs.getLong("employee_id"),rs.getLong("consignment_id"),rs.getString("status")));
    }

    public List<EmployeeTask> getAllByEmployeeId(Long employeeId){
        return jdbcTemplate.query(getAllByEmployeeId,new Object[]{employeeId},(rs, rowNum) -> new EmployeeTask(rs.getLong("id"),rs.getLong("employee_id"),rs.getLong("consignment_id"),rs.getString("status")));
    }

    public List<EmployeeTask> getAllByConsignmentId(Long consignmentId){
        return jdbcTemplate.query(getAllByConsignmentId,new Object[]{consignmentId},(rs, rowNum) -> new EmployeeTask(rs.getLong("id"),rs.getLong("employee_id"),rs.getLong("consignment_id"),rs.getString("status")));
    }

    public List<EmployeeTask> getAllByStatus(String status){
        return jdbcTemplate.query(getAllByStatus,new Object[]{status},(rs, rowNum) -> new EmployeeTask(rs.getLong("id"),rs.getLong("employee_id"),rs.getLong("consignment_id"),rs.getString("status")));
    }

    public List<EmployeeTask> getAllByEC(Long employeeId,Long consignmentId){
        return jdbcTemplate.query(getAllByEC,new Object[]{employeeId,consignmentId},(rs, rowNum) -> new EmployeeTask(rs.getLong("id"),rs.getLong("employee_id"),rs.getLong("consignment_id"),rs.getString("status")));
    }

    public List<EmployeeTask> getAllByES(Long employeeId,String status){
        return jdbcTemplate.query(getAllByES,new Object[]{employeeId,status},(rs, rowNum) -> new EmployeeTask(rs.getLong("id"),rs.getLong("employee_id"),rs.getLong("consignment_id"),rs.getString("status")));
    }

    public List<EmployeeTask> getAllByCS(Long consignmentId,String status){
        return jdbcTemplate.query(getAllByCS,new Object[]{consignmentId,status},(rs, rowNum) -> new EmployeeTask(rs.getLong("id"),rs.getLong("employee_id"),rs.getLong("consignment_id"),rs.getString("status")));
    }

    public List<EmployeeTask> getAllByECS(Long employeeId,Long consignmentId,String status){
        return jdbcTemplate.query(getAllByECS,new Object[]{employeeId,consignmentId,status},(rs, rowNum) -> new EmployeeTask(rs.getLong("id"),rs.getLong("employee_id"),rs.getLong("consignment_id"),rs.getString("status")));
    }

    public EmployeeTask getById(Long id){
        List<EmployeeTask> employeeTasks = jdbcTemplate.query(getById,new Object[]{id},(rs, rowNum) -> new EmployeeTask(rs.getLong("id"),rs.getLong("employee_id"),rs.getLong("consignment_id"),rs.getString("status")));
        if(employeeTasks.size() == 0)
            return null;
        return employeeTasks.get(0);
    }

    public void update(EmployeeTask employeeTask){
        jdbcTemplate.update(update,employeeTask.getEmployeeId(),employeeTask.getConsignmentId(),employeeTask.getStatus(),employeeTask.getId());
    }

    public void save(EmployeeTask employeeTask){
        jdbcTemplate.update(save,employeeTask.getEmployeeId(),employeeTask.getConsignmentId(),employeeTask.getStatus());
    }

    public void delete(Long id){
        jdbcTemplate.update(delete, id);
    }

    public void deleteByEmployeeId(Long employeeId){
        jdbcTemplate.update(deleteByEmployeeId, employeeId);
    }

    public void deleteByConsignmentId(Long consignmentId){
        jdbcTemplate.update(deleteByConsignmentId, consignmentId);
    }

    public void deleteByStatus(String status){
        jdbcTemplate.update(deleteByStatus, status);
    }

    public void deleteAll(){
        jdbcTemplate.update(deleteAll);
    }

    public void dropTable(){
        jdbcTemplate.update(dropTable);
    }
}
