package com.amissible.payment.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Config_payment_manager {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String org_name;
    private String org_alias;
    private String validity_from;
    private String validity_until;
    private String url_success;
    private String url_cancel;
    private String paypal_client_id;
    private String paypal_client_secret;
    private String register_timestamp;

    public Config_payment_manager(Long id, String org_name, String org_alias, String validity_from, String validity_until, String url_success, String url_cancel, String paypal_client_id, String paypal_client_secret, String register_timestamp) {
        this.id = id;
        this.org_name = org_name;
        this.org_alias = org_alias;
        this.validity_from = validity_from;
        this.validity_until = validity_until;
        this.url_success = url_success;
        this.url_cancel = url_cancel;
        this.paypal_client_id = paypal_client_id;
        this.paypal_client_secret = paypal_client_secret;
        this.register_timestamp = register_timestamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getOrg_alias() {
        return org_alias;
    }

    public void setOrg_alias(String org_alias) {
        this.org_alias = org_alias;
    }

    public String getValidity_from() {
        return validity_from;
    }

    public void setValidity_from(String validity_from) {
        this.validity_from = validity_from;
    }

    public String getValidity_until() {
        return validity_until;
    }

    public void setValidity_until(String validity_until) {
        this.validity_until = validity_until;
    }

    public String getUrl_success() {
        return url_success;
    }

    public void setUrl_success(String url_success) {
        this.url_success = url_success;
    }

    public String getUrl_cancel() {
        return url_cancel;
    }

    public void setUrl_cancel(String url_cancel) {
        this.url_cancel = url_cancel;
    }

    public String getPaypal_client_id() {
        return paypal_client_id;
    }

    public void setPaypal_client_id(String paypal_client_id) {
        this.paypal_client_id = paypal_client_id;
    }

    public String getPaypal_client_secret() {
        return paypal_client_secret;
    }

    public void setPaypal_client_secret(String paypal_client_secret) {
        this.paypal_client_secret = paypal_client_secret;
    }

    public String getRegister_timestamp() {
        return register_timestamp;
    }

    public void setRegister_timestamp(String register_timestamp) {
        this.register_timestamp = register_timestamp;
    }

    @Override
    public String toString() {
        return "Config_payment_manager{" +
                "id=" + id +
                ", org_name='" + org_name + '\'' +
                ", org_alias='" + org_alias + '\'' +
                ", validity_from='" + validity_from + '\'' +
                ", validity_until='" + validity_until + '\'' +
                ", url_success='" + url_success + '\'' +
                ", url_cancel='" + url_cancel + '\'' +
                ", paypal_client_id='" + paypal_client_id + '\'' +
                ", paypal_client_secret='" + paypal_client_secret + '\'' +
                ", register_timestamp='" + register_timestamp + '\'' +
                '}';
    }
}
