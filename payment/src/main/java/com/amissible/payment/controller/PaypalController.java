package com.amissible.payment.controller;

import com.amissible.payment.service.PaypalService;
import com.amissible.payment.exception.AppException;
import com.amissible.payment.model.Config_payment_manager;
import com.amissible.payment.model.Order;
import com.paypal.api.payments.Sale;
import com.paypal.api.payments.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import org.springframework.web.servlet.view.RedirectView;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class PaypalController {

	@Autowired
	PaypalService service;

	//public static final String BASE_URL = "http://localhost:8083/"; // TESTING
	@Value("${payment.url.base}")
	public String BASE_URL; // PRODUCTION

	public static final String SUCCESS_URL = "pay/success";
	public static final String CANCEL_URL = "pay/cancel";

	private static final Logger logger = LoggerFactory.getLogger(PaypalController.class);

	@RequestMapping("/")
	public String home() {
		return "home";
	}

	@RequestMapping("/index")
	public String index(ModelMap modelMap,
						@RequestParam String alias,
						@RequestParam String token,
						@RequestParam String currency,
						@RequestParam Double amount) {
		Config_payment_manager obj = service.getConfigFromDB(alias);
		SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd");
		Date from,today,until;
		try {
			from = sdfo.parse(obj.getValidity_from());
			today = sdfo.parse(sdfo.format(new Date()));
			until = sdfo.parse(obj.getValidity_until());
		}
		catch (Exception e){
			throw new AppException("Date format incorrect",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(from.compareTo(today) > 0)
			throw new AppException("System Date is incorrect", HttpStatus.INTERNAL_SERVER_ERROR);
		if(today.compareTo(until) > 0)
			throw new AppException("Service Expired", HttpStatus.NOT_EXTENDED);

		modelMap.addAttribute("org",obj.getOrg_name());
		modelMap.addAttribute("alias",alias);
		modelMap.addAttribute("token",token);
		modelMap.addAttribute("currency",currency);
		modelMap.addAttribute("amount",amount);
		System.out.println(BASE_URL);
		return "index";
	}

	@PostMapping("/pay")
	public String payment(@ModelAttribute("order") Order order) {
		try {
			Payment payment = service.createPayment(
					order.getPrice(),
					order.getCurrency(),
					order.getMethod(),
					order.getIntent(),
					order.getAlias(),
					order.getDescription(),
					BASE_URL + CANCEL_URL + "?alias="+order.getAlias()+"&baseToken="+order.getDescription(),
					BASE_URL + SUCCESS_URL + "?alias="+order.getAlias());
			for(Links link:payment.getLinks()) {
				if(link.getRel().equals("approval_url")) {
					return "redirect:"+link.getHref();
				}
			}
			
		} catch (PayPalRESTException e) {
			//e.printStackTrace();
			throw new AppException("API Exception Occurred",HttpStatus.EXPECTATION_FAILED);
		}
		return "redirect:/";
	}

	@GetMapping(value = CANCEL_URL)
	public RedirectView cancelPay(@RequestParam String alias,@RequestParam String baseToken) {
		Config_payment_manager obj = service.getConfigFromDB(alias);
		RedirectView redirectView = new RedirectView();
		redirectView.setUrl(obj.getUrl_cancel()+"?token="+baseToken);
		logger.info("PAYMENT FAILURE : "+alias+" {\"token\":\""+baseToken+"\",\"cause\":\"Payment Cancelled by Payer\"}");
		return redirectView;
	}

	@GetMapping(value = SUCCESS_URL)
	public RedirectView successPay(@RequestParam("alias") String alias,
								   @RequestParam("paymentId") String paymentId,
								   @RequestParam("token") String token,
								   @RequestParam("PayerID") String payerId) {

		Config_payment_manager obj = service.getConfigFromDB(alias);
		RedirectView redirectView = new RedirectView();
		String response = obj.getUrl_success();
		response += "?paymentId="+paymentId+"&ppToken="+token+"&PayerID="+payerId;
		try {
			Payment payment = service.executePayment(alias,paymentId, payerId);
			if (payment.getState().equals("approved")) {

				Transaction transaction = payment.getTransactions().get(0);
				Sale sale = transaction.getRelatedResources().get(0).getSale();

				String userToken = transaction.getDescription();
				response += "&token="+ userToken.substring(userToken.indexOf('-')+1);
				response += "&state=" + sale.getState().toLowerCase();
				response += "&currency=" + sale.getAmount().getCurrency();
				response += "&amount=" + sale.getAmount().getTotal();
				response += "&fees=" + sale.getTransactionFee().getValue();
				redirectView.setUrl(response);
				logger.info("PAYMENT SUCCESS : "+alias+" {\"token\":\""+userToken+"\",\"cause\":\""+response+"\"}");
				return redirectView;
			}
		} catch (PayPalRESTException e) {
			logger.error("PAYMENT FAILURE : "+alias+" {\"token\":\""+token+"\",\"cause\":\"API Error : "+e.getMessage()+"\"}");
		 	throw new AppException("API Exception Occurred",HttpStatus.EXPECTATION_FAILED);
		}
		return redirectView;
	}
}
